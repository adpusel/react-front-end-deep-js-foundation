function fakeAjax( url, cb ) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = ( Math.round( Math.random() * 1E4 ) % 8000 ) + 1000;

	console.log( "Requesting: " + url );

	setTimeout( function () {
		cb( fake_responses[ url ] );
	}, randomDelay );
}

function output( text ) {
	console.log( text );
}

// **************************************
// The old-n-busted callback way

function getFile( file ) {
	return new Promise( function ( resolve ) {
		fakeAjax( file, resolve );
	} );
}

//
//
// [
// 	"file1", "file2", "file3"
// ]
// .map( getFile )
// .reduce( function combine( chain, pr ) {
// 	return chain.then( function () {
// 		return pr.then( output );
// 	} );
// }, Promise.resolve() )
// .then( () => console.log( "Complete" ) );
//
// Promise.resolve()
// 			 .then( function () {
// 				 return p1;
// 			 } )
// 			 .then( console.log );

// const promiseData =
// 					data.map( () => getFile( "file" ) )
// 							.map( ( el ) => el.then( console.log ) );

// .then( ( data ) => console.log( data ) ) );
//
// const reduced = promiseData.reduce( ( data, el ) => data.then( () => el ), new Promise( ( resolve ) => resolve( null ) ) );

// Request all files at once in
// "parallel" via `getFile(..)`.
//
// Render as each one finishes,
// but only once previous rendering
// is done.

// ???

[
	"file1", "file2", "file3"
]
.map( getFile )
.reduce( ( chain, pr ) =>
				chain
				.then( () => pr )
				.then( output )
		,
		Promise.resolve()
)
.then( () => console.log( "end" ) );


var p = Promise.resolve();
Promise.race( [
	p,
	new Promise( function ( _, reject ) {
		setTimeout( () => reject( "Timeout!!" ), 3000 );
	} )
] );








