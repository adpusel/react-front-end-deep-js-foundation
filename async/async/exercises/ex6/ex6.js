function fakeAjax( url, cb ) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = ( Math.round( Math.random() * 1E4 ) % 8000 ) + 1000;

	console.log( "Requesting: " + url );

	setTimeout( function () {
		cb( fake_responses[ url ] );
	}, randomDelay );
}

function output( text ) {
	console.log( text );
}

// **************************************

function getFile( file ) {
	return ASQ( function ( done ) {
		fakeAjax( file, done );
	} );
}

ASQ()
.seq(
		...(
				[ "file1", "file2", "file3" ]
				.map( getFile )
				.map( ( sp ) => sp.val( output ) )
		)
)
.val( () => console.log( "Complete" ) );

// Request all files at once in
// "parallel" via `getFile(..)`.
//
// Render as each one finishes,
// but only once previous rendering
// is done.

// ???


function getData( d ) {
	setTimeout( () => run( d ), 1000 );
}

const coroutine = ( d ) => d;

var run = coroutine( function* () {
	// the iterator is call himself inside the
	// api call, that's awesome! really awesome.
	var x = 1 + ( yield getData( 10 ) );
	var y = 1 + ( yield getData( 30 ) );
	var answer = ( yield  getData(
			"Meaning of life: " + ( x + y )
	) );
	console.log( answer );
} );

run();


// chan is a channel message
const chan = () => null;

var ch = chan();

function* process1() {
	yield put( ch, "Hello" );
	var msg = yield take( ch );
	console.log( msg );
}

function* process2() {
	var greeting = yield take( ch );
	yield put( ch, greeting + " world" );
	console.log( "done!" );
}




















