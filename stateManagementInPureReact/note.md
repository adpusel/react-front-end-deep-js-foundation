# Introduction

#### What is state?
There a many kinds of states:
* __Model data__: The nouns in your application (product's name, price...).
* __View / UI state__: Are those nouns sorted in ascending or descending order?
* __Session state__: is the user logged.
* __Communication__: are we in fetch process?
* __Location__: save in the url some data (like with taskMaker).

We can simplify that by only two:
1. Model State : The data in the app like the items in a given list.
2. Ephemeral state: the value given by an input that just get hit.

## Spoiler Alert ##
There is no silver bullet!

## Class
setState is async, and react try rerender the less.
That's what setState do, merge all the call,
and the last one wins.
__If on big method, I call setState a lot,
I will got no warning, that not error!__
```javascript
Object.assign(
		{},
		firstCallSetState,
		SecondCallSetState
		...
)
```

So the next method increment only by one.

```javascript
increment() {
	this.setState({count: this.state.count + 1});
	this.setState({count: this.state.count + 1});
	this.setState({count: this.state.count + 1});
}
```
But if I use a function, I will increment by 3,
because now, react don't merge object.

~~~javascript
increment() {
	this.setState((state) => ({count: state.count + 1}))
	this.setState((state) => ({count: state.count + 1}))
	this.setState((state) => ({count: state.count + 1}))
}
~~~

I can use logic in how I update the state

~~~javascript
this.setState(state => {
	if (state.count >= 6) return;
	return {count: state.count + 1};
});
~~~

if now we want to pass some data to our Counter component:
I can mock and test easily with that 2nd pattern.
~~~javascript
// the component's call:
<Counter max={ 3 } step={ 2 }/>;

// increment first
increment() {
	const { max, step } = this.props;
	this.setState( state => {
		if ( state.count >= max ) return;
		return { count: state.count + step };
	} );
}

// the function
increment() {
	this.setState = ( ( state, props ) => {
		const { max, step } = props;
		if ( state.count >= max ) return;
		return { count: state.count + step };
	} );
}
~~~

Has this is __only a function__, I can use __out the component__
and __test__ out or use the __same logic__ in multiples component__s__!.

~~~javascript
const bestFuncEver = ( state, props ) => {
		const { max, step } = props;
		if ( state.count >= max ) return;
		return { count: state.count + step };
	} 
~~~

The second argument of setState, is a callback function
that will be trigger just after the update.
In the next example, if console.log in the updateFunction,
I only will get the value __before setState__.

~~~javascript
this.setState(
		updateFunction,
		() => {
			console.log(this.state)
		}
)
~~~

But the second function have no parameters,
so to take out the logic, I use clojure:

~~~javascript
const logAfterSetState = (state) => console.log(state)

this.setState(
		...,
		() => logAfterSetState(state)
)

// or

this.setState(
		...,
		logAfterSetState.bind(this)
)
~~~


## the setState pattern

I don't have the same pattern with state that I have with props;
1. don't duplicate data from props to state.
2. keep only variable that are use in render,
   because it's to rerender when needed that we put in state!

#### Dont'use for derivation of props
As the constructor is only trigger at the creation of the component,
if props change, the fullName will not be updated.
~~~javascript

class User extends Component {

	// wrong !!!!!!!!!!!!!!
	constructor(props) {
		super(props);
		this.state = {
			fullName: props.firstName + " " + props.lastName
		}
	}

	// gooooood
	get fullName() {
		const { firstName, lastName } = this.props;
		return firstName + ' ' + lastName;
	}
	
	render() {
		return (
				<h1>{ this.fullName }</h1>
		)
	}
}
~~~

It's better to only use function, in performance perspective and transform:

~~~javascript
class UserList extends Component {
	render() {
		const { users } = this.props;
		return (
				<section>
					<VeryImportantUserControls/>
					{ users.map( renderUserProfile ) }
				</section>
		);
	}

	renderUserProfile( user ) {
		return (
				<UserProfile
						key={ user.id }
						photograph={ user.mugshot }
				/>
		);
	}
}
~~~

in :

~~~javascript
const renderUserProfile = user => {
	return (
			<UserProfile
					key={ user.id }
					photograph={ user.mugshot }
			/>
	);
};

const UserList = ( { users } ) => {
	return (
			<section>
				<VeryImportantUserControls/>
				{ users.map( renderUserProfile ) }
			</section>
	);
};
~~~


# Hooks state
they create state, because to not rewrite component,
nobody use functional component.

~~~javascript

const [count, setCount] = React.useState(0)

const increment = () => setCount(count + 1);
const decrement = () => setCount(count - 1);
const reset = () => setCount(0)
~~~

It's async and have queue like,
count will only get + 1;

~~~javascript
function 3times() {
  increment(
  		setCount(count + 1);
  		setCount(count + 1);
  		setCount(count + 1);
  )
}
~~~

State Hooks can take function,
there is no props
it's only what there are in the function, here a number

in the second step, I up to +3
~~~javascript
setCount(c => c + 1);

function 3times() {
  setCount(c => c + 1);
  setCount(c => c + 1);
  setCount(c => c + 1);
}
~~~

But we loose the callback of our previous setState.

So we will useEffect aka sideEffects.

* no dependencies = run at every render.
* rerun for all dependencies in the array.

As we use functional component, we can't keep track
of our value, each render is a new function call.
so we use __Ref hook__.
Ref hook is both: an ref to dom node or a state that
keep track between render

React supply object that is :

~~~javascript
const refCounter = useRef()
// { current: null }

let message = ""
// here I compare with the previous value.
if (refCounter.current < count) message = "Higher";
refCounter.current = count;
~~~

But how clean up if I do interval or subscription?
I return a callBack that will clean after the effect;

~~~javascript
const count;

React.useEffect(() => {
	const id = setInterval(() => console.log(count), 300)
	// react will clean after that!
	return () => clearInterval(id)
}, [count])
~~~

# Reducer

Look at immutable.js to get cool handling
of array. Without it, I need to always return new reference,
array or object.

In the grudge list, when I rerender an element,
because I forgiven, that render all the list!

Reducer is really simple: it's function that take 2 arguments,
* the state
* the action
* return the new state.

so we will use the Hook : useReducer.
that divorce the state management out of the render component.
As we pull out the reducer, it's will be super easy to test it!

after setUp the reducer, we want to avoid rerender when it's
not needed with the use other Hook:
* useMemo => give function
* useCallback => give result


memo check for functional component if the
props have changed. But not put that everyWhere!


Performance gold :
__not doing stuff is faster than doing stuff__


what we get to pass on Context api:
* We lost all of our performance optimization with memo and useCallback
* But the code is much clean and simpler


## Data fetching









































