import React, { createContext, useCallback, useReducer } from 'react';
import id from 'uuid/v4';

import initialState from './initialState';

const GRUDGE_ADD = 'GRUDGE_ADD';
const GRUDGE_FORGIVE = 'GRUDGE_FORGIVE';

export const GrudgeContext = createContext( undefined, undefined );

const useUndoReducer = ( reducer, initialState ) => {
  const undoState = {
    past   : [],
    present: initialState,
    future : []
  };

  function undoReducer( state, action ) {

    const newPresent = reducer( state.present, action );

    const { past, present, future } = state;
    const { type } = action;

    if ( type === 'UNDO' ) {
      const [ newPresent, ...newPast ] = past;

      return {
        past   : newPast,
        present: newPresent,
        future : [ present, ...future ]
      };
    }

    if ( type === 'REDO' ) {
      const [ newPresent, ...newFuture ] = future;

      return {
        past   : [ present, ...past ],
        present: newPresent,
        future : newFuture
      };
    }

    return {
      past   : [ present, ...past ],
      present: newPresent,
      future : []
    };
  }

  return useReducer( undoReducer, undoState );
};


function reducer( state, { type, payload } ) {

  if ( type === GRUDGE_ADD ) {
    return [
      {
        id: id(),
        ...payload
      },
      ...state
    ];
  }

  if ( type === GRUDGE_FORGIVE ) {
    return state.map( grudge => {
      if ( grudge.id === payload.id ) {
        return { ...grudge, forgiven: !grudge.forgiven };
      }
      return grudge;
    } );
  }

  return { ...state };
}

export const GrudgeProvider = ( { children } ) => {
  const [ { present, past, future }, dispatch ] = useUndoReducer( reducer, initialState );
  const isPast = !!past.length;
  const isFuture = !!future.length;

  const addGrudge = useCallback( ( { person, reason } ) => {
    dispatch( {
      type   : GRUDGE_ADD,
      payload: {
        person,
        reason,
        forgiven: false,
        id      : id()
      }
    } );
  }, [ dispatch ] );

  const toggleForgiveness = useCallback( id => {
    dispatch( {
      type   : GRUDGE_FORGIVE,
      payload: { id }
    } );
  }, [ dispatch ] );
  const undo = useCallback( () => {
    dispatch( { type: 'UNDO' } );
  }, [ dispatch ] );

  const redo = useCallback( () => {
    dispatch( { type: 'REDO' } );
  }, [ dispatch ] );

  const value = {
    grudges: present,
    addGrudge,
    toggleForgiveness,
    undo,
    redo,
    isPast,
    isFuture
  };

  return (
      <GrudgeContext.Provider value={ value }>
        { children }
      </GrudgeContext.Provider>
  );
};

