import React, { useContext } from 'react';
import Grudges from './Grudges';
import { GrudgeContext } from './GrudgesContext';

import NewGrudge from './NewGrudge';

const Application = () => {
  const { undo, isPast, isFuture, redo } = useContext( GrudgeContext );
  return (
      <div className="Application">
        <NewGrudge/>
        <section>
          <button onClick={ undo } disabled={ !isPast }>Undo</button>
          <button onClick={ redo } disabled={ !isFuture }>Redo</button>
        </section>
        <Grudges/>
      </div>
  );
};

export default Application;
