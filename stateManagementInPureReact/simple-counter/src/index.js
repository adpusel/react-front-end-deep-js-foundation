import React from "react";
import { render } from "react-dom";

import "./styles.scss";

// the hook
const useLocalStorage = ( { initialState, key } ) => {

	const getFromStorage = () => {
		const storage = localStorage.getItem( key );
		const parsed = JSON.parse( storage )[ "value" ];
		if ( storage ) return parsed;
		return initialState;
	};

	const [ value, setValue ] = React.useState( getFromStorage() );

	React.useEffect( () => {
		localStorage.setItem( key, JSON.stringify( { value } ) );
	}, [ value ] );

	return [ value, setValue ];
};


const Counter = ( { max, step } ) => {
	const refCounter = React.useRef();
	const [ count, setCount ] = useLocalStorage( {
		initialState: 0,
		key         : "customCounter"
	} );

	let message = "";
	// I compare the previous value with the new one,
	// like in componentWillUpdate
	if ( refCounter.current < count ) message = "Higher";

	// here I save the current value for next comparing
	refCounter.current = count;

	const increment = () => setCount( c => {
		if ( c >= max ) return c;
		return c + step;
	} );

	React.useEffect( () => {
		const id = setInterval( () => console.log( `${ count }` ), 2000 );
		return () => clearInterval( id );
	}, [ count ] );

	const decrement = () => setCount( count - 1 );
	const reset = () => setCount( 0 );

	return (
			<main className="Counter">
				<h2>{ message }</h2>
				<p className="count">{ count }</p>
				<section className="controls">
					<button onClick={ increment }>Increment</button>
					<button onClick={ decrement }>Decrement</button>
					<button onClick={ reset }>Reset</button>
				</section>
			</main>
	);
};

render( <Counter max={ 5 } step={ 2 }/>, document.getElementById( "root" ) );
