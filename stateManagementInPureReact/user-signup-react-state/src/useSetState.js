import { useReducer } from 'react';

function reducer( previousState = {}, updatedState = {} ) {
  return { ...previousState, ...updatedState };
}

const useSetState = ( initialState ) => {
  const [ state, dispatch ] = useReducer( reducer, {} );

  const setState = updatedState => dispatch( updatedState );

  return [ state, setState ];
};

export default useSetState;
