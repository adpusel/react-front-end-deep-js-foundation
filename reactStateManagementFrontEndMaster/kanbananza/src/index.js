import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import rootReducer from './reducers/index';

import './index.scss';
import Application from './components/Application';

const store = createStore(
    rootReducer,
    applyMiddleware( thunk ),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

ReactDOM.render(
    <Provider store={ store }>
      <Application/>
    </Provider>,
    document.getElementById( 'root' ),
);
