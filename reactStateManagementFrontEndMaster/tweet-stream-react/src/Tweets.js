import React from 'react';
import { connect } from 'react-redux';

import Tweet from './Tweet';

const mapTweets = ( tweet ) => <Tweet key={ tweet.id } tweet={ tweet }/>;

const Tweets = ( { tweets = [] } ) => {
  return (
      <section className="Tweets">
        { tweets.map( mapTweets ) }
      </section>
  );
};

const mapStateToProps = ( { tweets } ) => ( { tweets } );

export default connect( mapStateToProps )( Tweets );

