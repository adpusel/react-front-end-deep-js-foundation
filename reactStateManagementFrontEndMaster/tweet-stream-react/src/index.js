import React from 'react';
import ReactDOM from 'react-dom';

import { createStore, applyMiddleware } from 'redux';
import { connect, Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { fetchTweets } from './actions';

import reducer from './reducer';

import FetchTweets from './FetchTweets';
import Tweets from './Tweets';

import './styles.scss';

const store = createStore( reducer, applyMiddleware( thunk ) );
export const { dispatch } = store;

const Application = () => {
  return (
      <div className="Application">
        <h1>Tweet Stream</h1>
        <FetchTweets fetchTweets={ fetchTweets }/>
        <Tweets/>
      </div>
  );
};


const rootElement = document.getElementById( 'root' );
ReactDOM.render(
    <Provider store={ store }>
      <Application/>
    </Provider>,
    rootElement,
);
