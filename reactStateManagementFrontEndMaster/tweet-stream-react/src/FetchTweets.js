import React from 'react';
import { connect } from 'react-redux';
import { dispatch } from './index';

const FetchTweets = ( { fetchTweets } ) => {
  return <button
      onClick={ () => dispatch( fetchTweets() ) }>Fetch Tweets</button>;
};

export default connect()( FetchTweets );
