function printRecords( recordIds ) {

	// see next code to catch the purpose of these functions
	const findId = current => recordIds.find( el => el == current.id );
	const cmpName = ( a, b ) => Number( a.name > b.name );
	const printStudent = ( student ) => console.log( `${ student.name } ${ student.id }` );

	( studentRecords.filter( findId ) )
	.sort( cmpName )
	.forEach( printStudent );

}

function paidStudentsToEnroll() {
	const isStudentPaid = el => el.paid;
	const extractId = ( { id } ) => id;

	const idsOfPaidStudent = studentRecords.filter( isStudentPaid )
																				 .map( extractId );
	return [ ... currentEnrollment, ... idsOfPaidStudent ];
}

function remindUnpaid( recordIds ) {

	const getStudendById = id => studentRecords.find( student => student.id == id );
	const filterPaid = id => getStudendById( id ).paid == false;
	const noPaid = recordIds.filter( filterPaid );
	printRecords( noPaid );
}


// ********************************

var currentEnrollment = [ 410, 105, 664, 375 ];

var studentRecords = [
	{ id: 313, name: "Frank", paid: true, },
	{ id: 410, name: "Suzy", paid: true, },
	{ id: 709, name: "Brian", paid: false, },
	{ id: 105, name: "Henry", paid: false, },
	{ id: 502, name: "Mary", paid: true, },
	{ id: 664, name: "Bob", paid: false, },
	{ id: 250, name: "Peter", paid: true, },
	{ id: 375, name: "Sarah", paid: true, },
	{ id: 867, name: "Greg", paid: false, },
];

printRecords( currentEnrollment );
console.log( "----" );
currentEnrollment = paidStudentsToEnroll();
printRecords( currentEnrollment );
console.log( "----" );
remindUnpaid( currentEnrollment );

/*
 Bob (664): Not Paid
 Henry (105): Not Paid
 Sarah (375): Paid
 Suzy (410): Paid
 ----
 Bob (664): Not Paid
 Frank (313): Paid
 Henry (105): Not Paid
 Mary (502): Paid
 Peter (250): Paid
 Sarah (375): Paid
 Suzy (410): Paid
 ----
 Bob (664): Not Paid
 Henry (105): Not Paid
 */
