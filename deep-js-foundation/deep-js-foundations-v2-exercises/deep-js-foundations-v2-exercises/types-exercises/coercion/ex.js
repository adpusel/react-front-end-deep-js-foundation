// TODO: write the validation functions

function isValidName( attempted ) {
	if (
			typeof attempted !== "string"
			&& attempted.trim().length >= 3
	) {
		return true;
	}
	return false;
}

function hoursAttended( attempted, length ) {
	attempted = convertIfString( attempted );
	length = convertIfString( length );

	if ( test_neg( attempted ) || test_neg( length ) ) {
		return false;
	}

	if ( typeof attempted != "number" || typeof length != "number"
			 || attempted < 0 || length < 0
			 || !Number.isInteger( attempted ) || !Number.isInteger( length ) ) {
		return false;
	}


	function convertIfString( val ) {
		if ( val == "string" && attempted.trim() != "" ) {
			return Number( val );
		}
		return val;
	}

	function test_neg( val ) {
		return ( val < 0 || Boolean( val ) === false );
	}

}


// tests:
// console.log( isValidName( "Frank" ) === true );
console.log( hoursAttended( 6, 10 ) === true );
console.log( hoursAttended( 6, "10" ) === true );
console.log( hoursAttended( "6", 10 ) === true );
console.log( hoursAttended( "6", "10" ) === true );
//
// console.log( isValidName( false ) === false );
// console.log( isValidName( null ) === false );
// console.log( isValidName( undefined ) === false );
// console.log( isValidName( "" ) === false );
// console.log( isValidName( "  \t\n" ) === false );
// console.log( isValidName( "X" ) === false );
console.log( hoursAttended( "", 6 ) === false );
console.log( hoursAttended( 6, "" ) === false );
console.log( hoursAttended( "", "" ) === false );
console.log( hoursAttended( "foo", 6 ) === false );
console.log( hoursAttended( 6, "foo" ) === false );
console.log( hoursAttended( "foo", "bar" ) === false );
console.log( hoursAttended( null, null ) === false );
console.log( hoursAttended( null, undefined ) === false );
console.log( hoursAttended( undefined, null ) === false );
console.log( hoursAttended( undefined, undefined ) === false );
console.log( hoursAttended( false, false ) === false );
console.log( hoursAttended( false, true ) === false );
console.log( hoursAttended( true, false ) === false );
console.log( hoursAttended( true, true ) === false );
console.log( hoursAttended( 10, 6 ) === false );
console.log( hoursAttended( 10, "6" ) === false );
console.log( hoursAttended( "10", 6 ) === false );
console.log( hoursAttended( "10", "6" ) === false );
console.log( hoursAttended( 6, 10.1 ) === false );
console.log( hoursAttended( 6.1, 10 ) === false );
console.log( hoursAttended( 6, "10.1" ) === false );
console.log( hoursAttended( "6.1", 10 ) === false );
console.log( hoursAttended( "6.1", "10.1" ) === false );
