# Introduction
== check value / loose
=== check value and type / strict

But in the specs we see that both check the type, and do different stuff with
that.

__if the type match there is absolutely no differences between both.__
=== return false at start, if the type are differents, 
 
it's slightly different with objects, if they're differents object,
both will return false 
```javascript
let toto = {'toto'}
let tata = {'tata'}
toto == tata // false
toto === tata // false
```
## specs of the ==
* null == undefined // true

* if one of the element is number, coerce the second one to number
```javascript
// the next are same
let toto = 'str'
let titi = 29;

if (Number(toto) === Number(titi){}
if (toto == titi) {}
```
* if one of the element is a primitive, and the other object, the object is 
  convert with the ToPrimitive(hint) algorithm.
```javascript
let number = 42
let array = [42] // "42"
// if (number == array)
// if (42 == "42") // 1 back to primitive
// if (42 === 42) // rules convert to number
```

### Summary ==
if the types are the same: ===
if null or undefined: equal
if non-primitives: ToPrimitive
Prefer: ToNumber

### Corner Case:
[] == ![] // true ! 
```javascript
let a = []
let b = []

// first algo: 
if (a == !b)
if ([] == false)
if ("" == false)
if (0 == false)
if (0 == 0) // yep that's true

// second
if (a != b)
if ( !(a == b) )
if ( !( false) )
```

* Boolean corner case
```javascript
let a = []

if (a) {} // only one coercion, [] is not on the falsy table
if (Boolean(a)) // same

if (a == true) // is false
if ("" == true)
if (0 == 1) // convert to number


if (a == false) // is true
if ("" == false)
if (0 == 0) // convert to number
```

### Avoid 
1. with 0 or "" or "  "
2. with non-primitive because coercion
3. == true or false, if really needed, use ===

# Conclusion
* == is not about comparision with unknown types
* == is about comparison with known types, 
     optionally where conversions are helpful
* If you know the type in comparison, == and === are identical.
* If the type are different, it's stupid using === because that will always fail
  the same is true in the opposite way, don't use the === if the type are the same
* It's faster to let JS do the conversion with the == that ===.
  but the === will be faster if the type are the same.
* the Author is more about, use the === if that made the code more readable
* short : === is to aware the code is at this place, uncertain.



