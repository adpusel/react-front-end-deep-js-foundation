# Introduction
Author says do coercion is good, and I need to understand what I'm doing 
in purpose of doing stuff well.
He says to be more
* the low typing of Js is what allow it to be multi-paradigm, that's a force
  not a weak
* We need to see the code like a way to communicate my idea, nothing more!
it's a more philological stuff rather than code stuff.

# Implicit Coercion
Implicit != Magic
Finally it's when it's dangerous, make coercion everywhere is a bad idea 
because, I will lose code quality. It's a security.

# Understanding features:
__this is not good__
"If a feature is sometimes useful and sometimes dangerous and if there is a 
better option then always use the better option.
The Good Part"

__this is good__
Useful      : when the reader focuses on what's important.
Dangerous   : when the reader can't tell what will happen
Better      : when the reader understands the code
 
