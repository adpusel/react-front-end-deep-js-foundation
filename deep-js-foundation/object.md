# Introduction
* Js is not a class system, it's oriented object system

## this keyword: 

* A function's this references the execution context for that call,
  determined entirely by how the function was called.
* this is build at run time, because of the dynamic context
* a 'this' aware function can thus have a differente context
  each time it's called, which makes it more flexible & reusable

```javascript
var teacher = "Kyle"

function ask(question) {
  console.log(teacher, question);
}

function otherClass() {
  var teacher = "Suzy"

  ask("why")
}

otherClass();
```
* Begin 

```javascript
function ask(question) {
  console.log(this.teacher, question)
}

function otherClass() {
	var context = {
		teacher: "Suzy"
	};
	ask.call(myContext, "Why?"); // Suzy Why?
}

otherClass();
```

* remember the building where I go up to find the scope witch contains my stuff
  this is another building, with other stuff based on the context that is create
  when I call my function.

### Implicit binding
Here the 'this' keyword is automatically bind with the object.
this referts to the object you call it from.
```javascript
var workshop = {
    teacher: "Kyle",
    ask(question) {
      console.log(this.teacher, question);
    }
}

workshop.ask("What is implicit binding?")
```
* other example of implicit binding
* in the second line, we create by our own the context
```javascript
function ask( question ) {
	console.log( this.teacher, question );
}

var workshop1 = {
	teacher: "toto", ask
}

var workshop2 = {
	teacher: "titi", ask
}

workshop1.ask("oups") // toto oups
ask.call(workshop1, "oups") // toto oups

workshop2.ask("oups") // titi oups
ask.call(workshop2,  "oups") // titi oups
```

In this example, we lost context because, 
ask is call in the setTimeout context,
and he doesn't have the workshop context
** setTimeout, call the function in the global js object: cd.call(window,ask)
```javascript
var workshop = {
  teacher: "Kyle",
  ask(question) {
    console.log(this.teacher, question)
  }
}

setTimeout(workshop.ask, 10, "lost this")
// undefined lost this

setTimeout(workshop.ask.bind(workshop), 10, "hard bound this")
// Kyle hard bound this
```

__so, pay attention to the this keyword and if it's to painful, use closure__

## the new keyword
here, 'new' invoke the function, it's the third way to invoke function,
and the new keyword will do: 
- the main purpose it to call the function with brand-new empty this
    * is like fu.call({}, arg...)
- Link that object to another object
- Call the function with the new object
- if function does not return, return this
```javascript
function ask(q) {
	console.log(this.teacher, q)
}

var newEmptyObject = new ask("What is 'new' doing here?")
// undefined What is ...
```

## Default binding
In strict mode, the default value of this,
if there is no intentional binding is undefined.
 
```javascript
var teacher = "Kyle"

function ask(question) {
  console.log(this.teacher, question);
}

function askAgain(question) {
  "use strict"
  console.log(this.teacher, question);
}

ask("what's the non-strict-mode default?")
// Kyle what's ...

askAgain("What's the strict-mode default?")
// typeError
```

## Binding precedence
The pattern of the determination of this keyword:
1. is the function called by new?
2. is the function called by call() or apply() ? _bind() use apply_
3. is the function called on a context object?
4. DEFAULT global object, except in strict mode.

```javascript
var workshop = {
  teacher: "Kyle",
  ask: function ask(question) {
    console.log(this.teacher, question);
  }
}

new (workshop.ask.bind(workshop))("What does this do")
// undefined What does this do
```

## Arrow function
* arrow function did not have this keyword
  inside then, the 'this' is a variable
  and js will search it in scopes.
* here, the scope of the next scope is ask function
  and the this of ask is set when I call it.
* I can't call new () => ... because arrow function doesn't have this keyword.
```javascript
var workshop = {
  teacher: "Kyle",
  ask(question) {
    setTimeout(() => {
      console.log(this.teacher, question); 
    }, 100)
  }
}

workshop.ask("is this lexical 'this'?")
// Kyle is this lexical this?
```

### Resolving this in arrow function
```javascript
var workshop = {
  teacher: "Kyle",
  ask: (question) => console.log(this.teacher, question)
}

workshop.ask("titi") // undefined
workshop.ask.call(workshop, "toto") // undefined

```
* this motto resume the 'this' concept 
__never do var self = this to save, but var context = this__

# The class system

* Class system is sugar syntax to the prototype system.
* Class add a bunch of complexity and are not just sugar syntax
  that is almost a language into a language
```javascript
class Workshop {
	contructor( teacher ) {
		this.teacher = teacher;
	}

	ask( question ) {
		console.log( this.teacher, question );
	}
}

var deepJs = new Workshop("Kyle")
var reactJs = new Workshop("Suzy")

deepJs.ask("Is 'class' a class?") // Kyle is ...
reactJs.ask("to") // Suzy to 

class AnotherWorkshop extends Workshop {
		speakUp(msg) {
			super.ask(msg)
		}
}

var JSRecentParts = new AnotherWorkshop("Kyle")

JSRecentParts.speakUp("are ...") // Kyle are ...
```

* but the core Js is still there, and loose binding is still something
```javascript
class Workshop {
	constructor( teacher ) {}

	ask() {console.log( this.teacher );}
}

var deepJs = new Workshop("Kyle")
setTimeout(deepJs.ask, 100, "loosing this")
// got undefined
```

## ES6 class

```javascript
class Workshop {
  constructor(teacher) {
    this.teacher = teacher;
    this.ask = question => console.log(question)
  }
}

var deepJs = new Workshop('Kyle')

setTimeout(deepJs.ask, 100, "Is 'this' fixed?");

```

We can't have the class method rely to the this because 
every method rely to the prototype not to the single instance.
so, it's not performance pb, it's we create an new version of module 
that is less efficient.














