# Introduction #################################################################
This guy is awesome, he creates a book that gets 114k on github ! 

## Understanding your code
To understand how the stuff works, I need to read the official documentation.
The documentation is wrote like an algorithm, and I know how that's work! 
to know where is the true. So here an example with the ++ operator : 

So here, will come the ++ algorithm: 
```javascript
function plusPlus(origin_x) {
  const origin_x_coerced = Number(origin_x);
  x = origin_x_coerced + 1;
  return origin_x_coerced;
}

let x = "5";
plusPlus(x)     // 5
console.log(x)  // 6
```
thesis lines of code are the implementation of the Specs. 
and it's important to see that we not return the new value, but the old one.

Whenever there's a divergence between what your brain thinks is happening,
and what the computer does, that's where bugs enter the code.

# Types ########################################################################
The type or coercion.
In Javascript, everything is an object is false, because false is not an
object :p, most elements behave as object.
A type is for say, I expect different behavior for these variables.
As illustration: 
    * object: 
        - object
        - function (object's son)
        - array (object's son)
    * not an object
        undefined
        string
        number
        bigint
        boolean
        symbol
        null


## Typeof operator
Typeof always return a short enum list of string and will never return other
thing, __never__.
```javascript
let v;
typeof v; // "undefined"
v = 4
typeof v // "number"
```

### Undefined 
Undefined is when there is no other value, like does not CURRENTLY have 
a value, undefined === no value yet 
```javascript
let v;
v = typeof v // "undefined"
```

### Null
Null is when an object isn't set,
```javascript
let v = null
v = typeof v // "object"
```

### Function
Typeof function return "function", and this isn't true, that type do not exist
but, if we break that, a lot of code will fail.
```javascript
let v = function() {}
v = typeof v // "function"
```

### Array
Is an object, and have a method to know what it is: array.isArray()
```javascript
let v = [1, 2, 3];
v = typeof // object
```

### BigInt
pay attention to check if bigInt and number, they can't be operate each other.

## Undefined vs undeclared vs uninitialized
* Undefined
    - Historically, Js return undefined even if a variable isn't set.
    - It exists, but is never used / set.
* Uninitialized
    - that an ES6 feature.
    - When a variable is not even undefined, because it's in the TDZ.
    - The variable is not reachable it's Uninitialized.
    - Is more 'hard' than undefined.
* Undeclared 
    - is when, in any scope, the variable didn't exist.
    - typeof is the only thing that can check without error 
      if that variable is undeclared.

## NaN and isNaN

### NaN
Basically that say, the variable is not a valid number.
NaN is __Number type__, is a number, just not a valid one.
It's a super thing, because in c I return -1 when I got an error, 
but it's a trick, here I can say with a number type: an error occurs.
```javascript

let myAge = Number("0o46") // octal for 38
let myNextAge = Number("39") // 39
let myCatAge = Number("n/a") // NaN
```

When Js see a minus sign, it needs number.
So tries to convert each element of the operation.
Any operation that includes NaN, will return Nan.
```javascript
let myAge = 22;
myAge = myAge - "my son's age" // NaN
```

__NaN is the only value doesn't equal to itself__, that's in the specs ! 
```javascript
let myNan = Number('toto');
let res = myNan === myNan // false !!
```

### IsNaN
Coerce the passed value with Number() before test it.
```javascript
isNaN("super toto") // true
```
To fix that in ES6
```javascript
Number.isNaN("super toto") // false
```

## Negative Zero
```javascript
let trendRate = -0;
trendRate === -0; // true

trendRate.toString() // "0" ?
trendRate === 0; // false !
trendRate < 0; // false
trendRate > 0; // false

Object.is(trendRate, -0) // true
Object.is(trendRate, 0) // false
```
The 0 can be utils, with the example of a car on a map,
the 0 is to get the direction of the car,
-0 allow remembering when the car stops.

There is a bug inside the Math.sign, we need to rewrite that function.
```javascript
Math.sign(-3); // -1
Math.sign(3); // 1
Math.sign(-0); // -0 ?? WTF?
Math.sign(0); // 0 ?? WTF

// fix that 
function sign(v) {
 return v !== 0 ? Math.sign(v) : Object.is(v, -0) ? -1 : 1;
}
```

## Fundamental Objects aka built-In Objects
Use new: 
* Object()
* Array()
* Function()
* Date()
* RegExp()
* Error()

Do not use new:
String()
Number()
Boolean()

```javascript
let yesterday = new Date("March 6, 2020");
yesterday.toUTCString();
// "Wed, 06 mar 2019 ..."
// !! there is no new, this is primitive, no need instantiation.
let myGPA = String(transcript.gpa);
// 3.54
```


# Coercion === Type conversion
When I add do some operations like add, or minus...
The javascript engine will try to convert with an algorithm
ToPrimitive(hint) variables used. 
when we have a non-primitive like array, or function and, we 
want to convert into a primitive.
The hint is number or string, if I do a + the hint will be number.
All non primitives type has that two functions : .valueOf and toString
that obviously return if possible the correct value.

## toString
* null == "null"
* undefined == "undefined"
* ...
* -0 == "0"

var.toString() == var.toPrimitive('string')

### Array serialization
* [] => "" yes empty string, without the []
* [1,2,3] => "1,2,3"
* [null, undefined] => ","
* [[], [],[[],[]]] => ",,,"
* [,,,] = ",,,"
Yeah it's shit... biwii

### Object serialization
* {} => "[object Object]"
* {a:2} => "[object Object]"
* {toString(){return "X";}} => "X"
    !! this one is overwriting the toString method, like with php when I set 
    a specific return during the piscine.

## ToNumber
on array and objects, there is no .toNumber, that switch directly to the .toString()

### Number Conversion

#### Cool stuff
"  "      0 ??
" 0 "     0
" -0 "    -0
" 009 "   9
" 3.222 " 3.222
" 0. "    0
" .0 "    0
" . "     NaN
" 0xaf "  175

#### Bad
false     = 0
true      = 1
null      = 0
undefined = NaN

### Array coercion
All the element inside that array become an empty string 
that become 0
[""] = 0
["0"] = 0
["-0"] = -0
[null] = 0
[undefined] = 0
[1,2,3] = NaN

### ToBoolean
That algorithm does not look up like toString // toNumber
all that is not in the Falsy list is true

#### All the Falsy value:
""
0, -0
null
NaN
false
undefined

#### All the rest is Truthy

## Coercion
I think I do not coercion but that false, I do that all the time 
I just made a coercion! 
```javascript
let numStudent = 16;
console.log(`There are ${numStudent} students.`)
```

### Operator Overloading

#### For the String
Here, specs say if one of the operand of + is a string, + do string concatenation,
so, it will call the .toString on the other !
```javascript
let start = "There are ";
let num = 16;
let end = " students."
console.log(start + num + end)
```
In the two previous example, an automatic conversion is create,
and that bad, because if they're primitives value, they didn't dave the method.
so the best way is, but of course for the log, that's overkill.
```javascript
let numStudent = 16
console.log(`There are ${String(numStudent)}`)
```

#### For Number

##### Plus : +
In that example, I will convert the result with .toValue = toPrimitive(number)
if the value is an number, everything will be cool and simple but if I have 
an empty string, I will get 0.
```javascript
function addAStudent(numStudents) {
  return numStudents + 1;
}
// here a bug can be
addAStudent(
  + studentInputElement.value
)
// 
addAStudent(
Number(input.value)
)
```

#### minus
Minus is only for numbers, so there is less risks

#### For Boolean
here, if a string has only "" it will be evaluated has true.
```javascript
// if the string is "" I well get true so
if (studentRecords.value) {
  numStudent = Number(studentRecords.value)
}
// here I check also if the string is empty
if(!!studentRecords.value)
{}


// here I loop until that is true, but each time there is coercion and I take
// risk.
while (newStudents.length) {
  enrollStudent(newStudents.pop())
}
// here I compare with a number, a number with a number
while (newStudents.length > 0) {}
```

the author say it's ok for him to check with implicit coercion for that: 
let var :object
if (var) {}
because null and undefined are the same in Boolean

Authors say, conversion are good and not a bad thing, with the example of 
accessing element inside like array, that are not object

### Coercion corner cases
The root of All : before doing coercion String in Boolean,
the Js engine, trim them. 
```javascript
studentRecords.value = "" // false
studentRecords.value = "     \t \n" // false
```

#### never compare Boolean with number
If I compare Boolean with Number, Js will convert the Boolean to 0 or 1
and like in this example bug will show up.
```javascript
Number(false) // 0
Number(true) //  1

1 < 2;      // true
1 < 2 < 3   // true
(true) < 3
1 < 3

3 > 2 > 1 // false
(true) > 1
1 > 1
```

