# Introduction
the scope is on of the foundation of js, it's good and powerful !
definition: Scope = __where to look for things__
It's like buckets with different color

* Js is parse, so it's no go line by line, Bash is a true interpret language,
  it will crash at the precise line where there is an error
  we have to think js like a two passes engine.
* Js organizes scopes with functions and blocks

```javascript
var teacher = "Kyle"

function otherClass() { // that a new scop
  var teacher = "Suzy" // that shadowing, I can't now access to Kyle
  console.log("welcome") // console is a global object! 
}

function ask() {
	var question = "Why"; // source mode => target mode
    console.log(question)
}

otherClass(); // source mode
ask();
```

```javascript
var teacher = "Kyle"

function otherClass() {
	teacher = "Suzy"
    topic = 'React' // if there is no var, the global scope will build one
    console.log("Welcome")
}

otherClass()
teacher
topic
```
```javascript
var teacher = "Kyle"

function otherClass() {
  var teacher = "Suzy"
  
  function ask(question) {
    console.log(teacher, question) 
  }
  ask("Why?")
}

otherClass();
ask("toto") 
```

undeclared = doesn't exit

think the scope like a building, I start at the first floor
I search in each floor, and the last one is the global state.

```javascript
function teacher() {}

var myTeacher = function anotherTeacher() {
 console.log(anotherTeacher) 
}

console.log(teacher)
console.log(myTeacher)
console.log(anotherTeacher)  // that exit only in his own scope
```

why anonymous function are bad
* can't use self reference to that function
* more debug stack trace, I see the name in the trace.
* more readable code, the function name help.
* that the same with arrow function, that are anonymous.

```javascript
// in promise: 
getPerson()
.then(function getDataFrom(person) { return person.id } )
.then(renderData);
```




