# Introduction
Think the lexical scope like, what's happen during compilation.
lexical scope = compiler.
bash is dynamic scope because it's truly interpreting language

```javascript
var teacher = "Kyle"

function otherClass() {
  var teacher = "Suzy"
  
  function ask(question) {
    console.log(teacher, question)
  }
}
```
Remember when he talks about the plugin on sublim text that allow to
color all the code with the color of its scope

```javascript
var teacher = "Kyle"

// here I create a new scope to avoid a naming collusion

// I got a new name that can do new collusion
( function anotherTeacher() {
    var teacher = "Suzy"
    console.log(teacher)
} )()

console.log(teacher)
```
- the code above allow to create a function one time inside it's scope,
  like when I do the same in c with the {} 
- the () transform the declaration into an expression, so no name is create
    * I can use void, +, - ... to turn that into an expression

There is a fundamental principe in IT, expose the least I can,
in defensive posture. With that I solve 3 main problems:
* Reduce area for the naming collusion.
* If that's hide, no one can mess up with.
* I protect myself for future bad refactoring.

Other usage for IIFE
```javascript
var teacher;

try {
	teacher = fetchTeacher( 1 );
}
catch ( e ) {
	teacher = "Kyle";
}

// now I can return the value with elegant way
let teacher = (
		function getTeacher() {
			try {
				return fetchTeacher( 1 );
			}
			catch ( e ) {
				return "Kyle";
			}
		}
)();
```

### Block scoping:
* var is attaches to the global scope, and can use everywhere.
* so the let and const that are attaches to block scope will create.
* the block turn into scope only if there is let / const in it
```javascript
var teacher = "Kyle"

{
	let teacher = "Suzy"
    console.log(teacher) // Suzy
}

console.log(teacher) // "Kyle"
```
* Var are not the evil, var is to say that variable is sharing between the scoop.
* Let / const is to say: that variable will never go outside.
* I should only used this feature, when it's obvious to keep in the block that variable.
```javascript
function diff( x, y ) {
	if ( x > y) {
		let tmp = x;
		x = y;
		y = x
	}
	return x - y;
}
```
* is let the new var ? no that's a very shitty advice
```javascript
function reapeat( fn, n ) {
	var result;
	for ( var i = 0; i < n; i++ ) {
		result = fn( result, i )
	}
    return result;
}
```

### Choosing let or var:
* Use let only to say at the reader, man the next stuff is scope only!
* the var is for all the other case.

```javascript
function reapeat( fn, n ) {
	var result;
    // I not see pb to use let here, I got why he says that
    // but, 
	for ( let i = 0; i < n; i++ ) {
		result = fn( result, i )
	}
    return result;
}
```
* here with better obvious example.
* Author think it's better to keep the assignation near the declaration.
* I say to the reader, I always return a value, semantically.
* Yeah, that true, the var allow more readable code here :).
* Author, when the function is bigger, when it's logic add again the var keyword
  to recall that this var belongs to that scope.
* the two var, point to the same memory location, one scope, one name, same place
```javascript
function lookupRecord(searchStr) {
  try {
    var id = getRecord( searchStr )
  } catch (e) {
    var id = -1
  }

  return id;
}
```

### Explicit let Block
```javascript
function formatStr( str ) {
	{	let prefix, rest;
		prefix = str.slice( 0, 3 );
		rest = str.slice( 3 );
		str = prefix.toUpperCase() + rest;
	}

	if ( /^FOO:/.test( str ) ) {
		return str;
	}

	return str.slice( 4 );
}
```

### const
First time I'm agree with the author, const is a variable that will not be 
reassign, but it's ok to const multiple stuff one after other.
like with an array or with the obj props. But it's different with typescript.

### Hosting
Hosting literally does not exist in the specs, because, it's not a real thing
It's serves to express the idea of lexical scope.
Hosting is a simply way to explain without hard thinking the AST and parser... 
```javascript
// here it's with parsing that Js engine will find where the variable are, or aren't
student;
teacher;
var student = "you"
var teacher = "Kyle"

// other example
var teacher = "Kyle";
otherTeacher();

function otherTeacher() { 
    console.log(teacher);
    var teacher = "Suzy";
}
```

* Look when hosting is cool
```javascript
// bad, we can't know when the assigment append!
teacher = "Toto"
var teacher

// function hoisting, Yeah cool man!
getTeacher();

function getTeacher() {
 return teacher 
}
```

### let doesn't hoist
```javascript
teacher = "Kyle";
let teacher; // TDZ error | Temporal Dead Zone
```
* the second fail, because, when Js see a var assignation
  it's create for the current scope an undefined variable
* let also create a variable for its scope, but don't initialise it.
  uninitialised == you can't touch it. 
```javascript
var teacher = "Kyle"

{
	console.log(teacher); // TDZ error
    let teacher = "Suzy"
}
```

* What's Temporal Dead Zone ?
It's exist because of const, that's protect to access the const's value before
it was assign, TDZ == the value is not ready. So they use also for guarding let

