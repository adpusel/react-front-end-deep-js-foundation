# Prototype
A class is the plan, not instancied, that's a start.
An instance is the building create according to the plan.

even, for performance reason, if there is some heritage, a the child instace 
will get a copy of all his parent method. Because it's faster than look up
in the class hierarchy.

All object that we use are created by the constructor calls, 
it's append when I use the new keyword.

A constructor call makes on object linked to its own prototype.
there is no copy, so make class, in a classic way doesn't fit with Js


the old way to create a prototype class
that is what happen in reality if that is 
write in sugar syntax
```javascript
function Workshop( teacher ) {
	this.teacher = teacher;
}

Workshop.prototype.ask = function ( question ) {
	console.log( this.teacher, question );
};

var deepJs = new Workshop( "Kyle" );
var reactJs = new Workshop( "Suzy" );

deepJs.ask( 'Is prototype a class?)
```

## The prototype chain
```javascript
function Workshop(teacher) {
  this.teacher = teacher
}
Workshop.prototype.ask = function(question) {
  console.log(this.teacher, question)
}

var deepJs = new Workshop("Kyle");

deepJs.constructor === Workshop; // true

deepJs.__proto__ === Workshop.prototype // true
Object.getPrototypeOf(deepJs) === Workshop.prototype // true
```

* arrow function does not have prototype, for that we can't use new with them.
* i can use super outside the class sugar syntax.

# Shadowing prototype
do kind of heritage with this code, _without class system_
is hell.
```javascript
function Workshop( teacher ) {
	this.teacher = teacher;
}

Workshop.prototype.ask = function ( question ) {
	console.log( this.teacher, question );
};

var deepJs = new Workshop( "Kyle" );

deepJs.ask = function(question) {
  this.ask(question.toUpperCase());
};

deepJs.ask("Oops, is this infinite recursion?")
```

## Prototypal Inheritance 
```javascript
function Workshop( teacher ) {
	this.teacher = teacher;
}

Workshop.prototype.ask = function ( question ) {
	console.log( this.teacher, question );
};

function AnotherWorkshop( teacher ) {
	Workshop.call( this, teacher );
}

AnotherWorkshop.prototype = Object.create( Workshop.prototype );
AnotherWorkshop.prototype.speakUp = function ( msg ) {
	this.ask( msg.toUpperCase() );
};

var JsRecentParts = new AnotherWorkshop( "Kyle" );

JsRecentParts.speakUp( "is this actually inheritance?" );
```

## Classical vs Prototypal Inheritance
In js, there is only linkage between the stuff.
instance are linked to their function, and the 
function are linked to the 'father' prototype
Remember with the scheme with class system and prototypal system

## Js Behavior delegation
one sentence : I can build a class system on a prototype system,
I can't do the reverse

## The OLOO Object Linked to Other Objects
```javascript
var Workshop = {
	setTeacher( teacher ) {
		this.setTeacher = teacher;
	}
	ask( question ) {
		console.log( this.setTeacher, question );
	}
};

var AnotherWorkshop = Object.assign(
		Object.create( Workshop ),
		{
			speackUp( msg ) {
				this.ask( msg.toUpperCase() );
			}
		}
);

var JSRecentParts = Object.create(AnotherWorkshop);
JSRecentParts.setTeacher("Kyle");
JSRecentParts.speackUp("cool buddy !")

// I do the same in the previous code snippet.
Object.create = function(o) {
  function F() {}
  F.prototype = o
  return new F();
}
```






