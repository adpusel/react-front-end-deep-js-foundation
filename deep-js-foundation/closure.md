# Closure

Doug, thinks all great idea take a generation to be implemented, popular.
Because we have to patient all the previous generation die or retire.
And closure are very good example, because we have to wait 2 generations.
It will introduce in js in 90's. Lisp is __full of closure__.
Brendan Eich is the creator of js.
Closure a so fat, that we can't give a definition of them !

Closure is when a function "remembers" its lexical scope,
even when the function run outside lexical scope.

```javascript
function ask(question) {
 setTimeout(function waitASec() {
  console.log(question); 
 }, 100) 
}

ask("What is closure?")
// or 
function ask2(question) {
 return () => question 
}

const toto = ask2("super") // I keep here access to question, the full ptr
```
so closure preserve access to the variable.

```javascript
for (var i = 1; i <= 3; i++) {
  setTimeout(() => console.log(`i: ${i}`), i * 1000)
}
```
It prints 4 4 4, because all the timeout callback i are the same varible, 
witch was increment to 4.

same with the Es6 feature Block: 
with the let keyword, a new var is create each loop, so I did not need that
all the time!
`for, for in, for of` do the same.
```javascript
for (let i = 1; i < 3; i++) {
  setTimeout(() => console.log(`i: ${i}`), i * 1000)
}
```

### Module pattern
That's not a module, that's a namespace pattern.
That's not bad, but definitely not a module.
That's not a module because module allow hide stuff.
```javascript
var workshop = {
	teacher: "Kyle",
	ask( question ) {
		console.log( this.teacher, question );
	}
};

workshop.ask( "Is this a module?" );
```

The idea of module, is there is 2 Api, the hide and the public.
There is an idea of visibility.

#### Definition
Modules encapsulate data and behavior (methods) together.
The state (data) of a module hold by its methods via closure.
```javascript
var workshop = ( function Module( teacher ) {
    // because here we create a closure for ask, the singleton module, will
    // stay alive all the time.
	var publicApi = { ask };
	return publicApi;

	function ask( question ) {
		console.log( teacher, question );
	}
} )( "Kyle" );

workshop.ask( "It's a module right?" );
// Kyle It's module, rigth?
```
* the purpose of a module is to hide and share a state between its content
* if there is no state to share, that's over engendering stuff.
* Here are a very good example of least exposing stuff.

The next example will not use the singleton pattern
```javascript
function WorkShopModule( teacher ) {
	var publicAPI = { ask };
	return publicAPI;

	function ask( question ) {
		console.log( teacher, question );
	}
}

var workshop = WorkShopModule('Kyle')

workshop.ask("It's a module, right?")
// Kyle It's a module, right?
```

### ES6 Modules
* Author think there are : work in progress: 
    * node.js and Js specs will not speak each other and, nodeJs, not handle
      well the es6 module.
    * specs say :
        - I need to write specific file .mjs
        - The first phase / 4, will be for the Q1 2020, and the syntax can change
        - So the Js transpiler will convert my Es6 style into interpretable nodeJs
          code.
```javascript
// workshop.mjs
var teacher = "Kyle"

export default function ask(question) {
  console.log(teacher, question);
}
```

* Es6 module are file associated, it's impossible to have 2 module in the same
  file.
* Es6 module are singleton.

#### Import them
```javascript
// workshop.mjs
var teacher = "Kyle"

export default function ask(question) {
  console.log(teacher, question)
}

// otherFile.js
import ask from "workshop.mjs"
ask("it' a cool sentence rigth?")

import * as workshop from "workshop.mjs"
workshop.ask("It's also a cool sentense")
```















