function upper( strings, ... values ) {
	let output = "";

	for ( let i = 0; i < strings.length; i++ ) {
		output += strings[ i ];
		if ( values[ i ] ) {
			output += values[ i ].toUpperCase();
		}
	}

	return output;
}

var name    = "kyle",
		twitter = "getify",
		topic   = "JS Recent Parts";


console.log(
		upper`Hello ${ name } (@${ twitter }), welcome to ${ topic }!` ===
		"Hello KYLE (@GETIFY), welcome to JS RECENT PARTS!"
);
