var numbers = {
	* [ Symbol.iterator ](
			start = 0,
			end   = 100,
			step  = 1
	) {
		for ( let i = start; i < end; i += step ) {
			yield `My lucky number are: ${ i }`;
		}
	}
};


// should print 0..100 by 1s
for ( let num of [ ... numbers[ Symbol.iterator ]( 1, 29, 2 ) ] ) {
	console.log( num );
}

// should print 6..30 by 4s
console.log( "My lucky numbers are: ____" );

// Hint:
//     [...numbers[Symbol.iterator]( ?? )]
