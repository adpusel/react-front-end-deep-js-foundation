////////////////////////////////////////////////////////////////////////////////
function data() {
	return [ 1, 2, 3, 4, 5 ];
}

var tmp = data();
var first = tmp[ 0 ];
var second = tmp[ 1 ];
var third = tmp[ 2 ];

// *****
var tmp;

let second; // I can declare my value before, destructuring is assignement
						// not declaration

var [
			first,
			second = 10, // that's a !==, if the value is null, that will pass and be = null
			third,
			... endOfTab // always return an array
		]        = tmp = data();
