# sync-async (with generators)
A generator can pause himself between the yield

```javascript
runner( function *(){
  var user = yield fetchCurrentUser();
  
  var [ archivedOrders, currentOrders ] = 
    yield Promise.all([
      fetchArchivedOrders( user.id ),
      fetchCurrentOrders( user.id )
    ])
})

// better

async function main() {
  var user = await fetchCurrentUser();
  
  var [ archivedOrders, currentOrder ] = 
    await Promise.all(...)  
}

main()
```

# async iteration
the forEach do not patient the end of promise, and add a async inside,
is add complication, because I will return / need a promise
```javascript
async function fetchFiles(files) {
  var prs = files.map( getFile );
  
  for (let p of prs)
  {
     console.log(await p)
  }  
}
```

# async generator
this will return a promise has iterator result
that's an asyncronis iterator. 
I got each time promise

```javascript
async function *fetchURLs( urls ) {
	for ( let url of urls ) {
		let resp = await fetch( url );
		if ( resp.status == 200 ) {
			let text = await resp.text();
			yield text.toUpperCase();}
		else {
			yield undefined;
		}}
}

async function main(favoriteSites) {
  for await (let text of fetchURLs( favoriteSites )) {
    console.log(text); 
  }
}
```





















