# Introduction

As block pattern is used with let, assign with destructuring is :
```javascript
function data() {
  return {b: 2, c: 3, d: 4} 
}

var first, second;

({
  b: second,
  a: first
} = data())

// or

var tmp

tmp = {
  b: second,
  a: first
}
```

use eslint to enforce always use default in my nested pattern.

the default value of the destructuring is only trigger if there is undefined value,
so always do that:
```javascript
let tmp = a
let b = { 
  a : b = 'toto',
  c : first = 1 } = tmp || {}
``` 

if I have destructuring argument in my function, better to have only that
as parameter.


that allow using named argument in Js
```javascript
function x( {
	store = "store",
	id = -1
} ) {}

x( { id: 2 } );

var defaults = ajaxOption()

var settings = {
  url: "toto",
  data: 42,
  callback : () => 1
}

ajax(ajaxOption(settings))

function ajaxOption( {
	url = "http://shit.fr",method = "post",
	data,
	callback,
	headers: [
		headers0 = "Content-Type: text/plain",
		... otherHeaders] = []
} = {} ) {
	return {
		url, method, data, callback,
		headers: [
				headers0,
				...otherHeaders
		]
	}
}
```












