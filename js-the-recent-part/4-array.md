if the array is complicated :
```javascript
var arr = [ { a: 1 }, { a: 2 } ];

arr.find( function match( v ) {
	return v && v.a > 1;
} );
// the pb is that method return undefined if it found nothing

// so use this one that return -1 if not found
arr.findIndex( function match( v ) {
	return v && v.a > 10;
} );
```

if the array is simple
```javascript
var arr = [10, 19, 39, NaN]

arr.includes(20) // false
arr.includes(NaN) // true
arr.includes(10, 4) // false
```


# Flat and Flat map:
```javascript
var nested = 
    [ 1, [2, 3], [[]], [4, [5] ], 6];

nested.flat(0);
// the same

nested.flat(1);
// [1, 2, 3, [], 4, [5], 6]

nested.flat(2)
// all flat my friend
```
so if I give him nested.(Infinity), I will flatten all possible array.


# flatMap
```javascript
[1,2,3].map(function tuples(v) {
  return [v * 2, String(v * 2)]
)}
// [ [2, "2"], [4, "4"], [6, "6"] ]

// I can flat it => one array
// or do all in one time
// and that's better performance because
// you not get some intermediates arrays
// but I got one level max
[1,2,3].flatMap(function all(v) {
  return [ v * 2, String(v * 2)]
})

// and that can also be used to delete some stuff of the array:
[1, 2, 3].flatMap((v) => {
  if (v % 2 == 0) {
    return [v, v * 2]; 
  }
  else {
    return [] 
  }
})
```











