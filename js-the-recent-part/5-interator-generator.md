# interator allow to iterare on almost all stuff in js
for of take only iterable stuff.

```javascript
var str = "Hello";
var it = str[Symbol.iterator]();

for (let v of it) {
  console.log(v);
}

// is the same that

for (let v of str) {
  console.log(v)
}

// I can also use ... , it consumes an iterator
var = "Hello";

var letters = [...str];
letters // [ "H", "e" ...]
```

What about objects that don't have iterator
```javascript
var obj = {}

for (let v of obj) {
  console.log(v)
} // TypeError
```

Search how set up iterator on the internet, in purpose of iterate on that object,
And, that object will be iterable by anyone with the current api. Because there are
20 lines to do that.

Here a way to do that better :
the * indicate we're dealing with a special function calls generator.
```javascript
function *main() {
  yield 1;
  yield 2;
  yield 3;
  return 4;
}

var it = main();

it.next(); // { value: 1, done: false }
it.next(); // { value: 2, done: false }
it.next();
it.next() // { value: 4, done: true }


the 4 has diseapear, because I return it, 
I can get only the value that I yield
[...main()] // [ 1, 2, 3 ]
```





