# New regex stuff
assertion, look ahead and look behind
```javascript
var msg = "Hello World"

msg.match(/(l.)/g)
// ["ll", "ld"]

msg.match(/(l.)$/g)
// ["ld"]

// that's a positive look ahead
msg.match(/(l.)(?=o)/g)
// ["ll"]

// negative lock ahead
msg.match(/(l.)(?!o)/g)
// ["ll", "ld"] // there are not follow by 'o'


// *************  look behind

// positive
msg.match(/(?<=e)(l.)/g)
// ["ll"]

// negative
msg.match(/(?<!e)(l.)/g)
// ["ll", "ld"]
```

# Named Capture Group
```javascript
var msg = "Hello Word"

msg.match(/.(l.)/g)
// ["ell", "ll"] I create a group inside the group

msg.match(/(?<cap>l.)/).groups
// {cap: "ll"}

msg.match(/(?<cap>[jklj])o Wor\k<caps>/);
// "lo worl" "l"

msg.replace(/(?<cap>l./g, "-$<cap>-");

// the first match block new match after!
msg.replace(/(?<cap>l.)/g, function(...argv) {
  var [,,,,{cap}] = argv; 
  return cap.toUpperCase()
})
// HeLLo worLD
```

# Dot all:
Due to historical, the dot [ . ] operator in js is unable to match the \n
but with that new flag, we now can !
```javascript
var msg = `
The quick brown fox
jumps over the
lazy dog
`

msg.match(/brown.*over/)
// null

msg.match(/brown.*over/s)
// [ "brown fox/n jumps over" ]
```













