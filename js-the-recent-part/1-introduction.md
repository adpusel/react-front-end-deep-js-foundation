# Javascript Fatigue
Js has start to evolve when people get stuck by IE,
at this century, there was different Js, the IE's, the firefox's and so one.
So smart people create One Js.

The es5 got 16years to build and introduce 300 new features.
So, EMGA decides to not do the same, and now a new version of the language
came each year.

With Js, you need to pay a lot of attention to all new features,
because nothing can be take out, we don't want to break site!

## Javascript move to Declarative
That make code communicating better, 
the new part, is more about the reader than the compiler.
That cool to evolve to keep utility of that language at his max.

## Browser support
Transpiler allow to select the good Js version in function of the browser.
There are now obligatory because there is a gap between the creation of new
feature, and the browser support.

## What we can expect from this course:  
recent part = since ES6


# Template String
like with styled-component: 
Here formatCurrency is a function, and I ask.
That's a way to preprocessing string. 
```javascript
var amount = 12.3;

var msg = formatCurrency`
The total for your order is ${ amount }`;

function formatCurrency( strings, ... value ) {
	var str = "";

	for ( let i = 0; i < strings.length; i++ ) {
		if ( i > 0 ) {
			if ( typeof value[ i - 1 ] == "number" ) {
				str += `$${ value[ i - 1 ].toFixed( 2 ) }`;
			}
			else {
				str += value[ i - 1 ];
			}
		}
		str += strings[ i ];
	}
}
```
Tag function can return anything, so people create tag function that return
regex, or css or whatever. That a language inside the language!

## String padding / String trimming
Padding need to be universal, because there are others way to read thad left to 
right. 
```javascript
let str = "Hello"

str.padStart(8, "*") // [string's length wanted, replacement char]
// ***Hello
str.padEnd(0)
```






















