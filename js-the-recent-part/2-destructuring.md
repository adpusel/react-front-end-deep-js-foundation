# Destructuring
Kyle says that's a hard stuff.
__decomposing a structure into its individual parts__

## iterative way
```javascript
var tmp = getSomeRecords(); 

var first = tmp[0]
var second = tmp[1]

var firstName = first.name;
var firstEmail = first.email !== undefined ?
    first.email :  
    "nobody"
var secondName = second.name;
var secondEmail = second.email !== undefined ?
    second.email :  
    "nobody"
```

## Destructuring Way
Pattern : variableName = defaultValue
```javascript
let [
			{
				name : firstName,
				email: firstEmail = "nobody"
			},
			{
				name : secondName,
				email: secondEmail = "nobody"
			}
		] = getSomeRecords();
```

I can literally do all assignation types with destructuring!
```javascript
var tmp = data()
var o = [];
[ o[1], o[22], ...o[100]] = tmp
```

I can have empty position with  , , and that switch the element.
```javascript
var tmp = data()
var o = [];
[ o[1], 
  , 
  ...o[100]] = tmp
```

swapping example:
```javascript
var x = 10;
var y = 20;
{
	let tmp = x;
    x = y
    y = x
}

// es6 example: 
[y, x] = [y, x]
```

protect my destructuring with ||,
and can also do that in the function call
```javascript
function data() {
  return null 
}

var [
    first,
    second,
    third,
    ] = data() || []
```

destructuring inside destructuring:
```javascript
function data() {
  return [1, [2, 3], 4]
}

var [
    first,
    [
      second,
      third,
    ] = [],
    fourth
] = data() || []

```









