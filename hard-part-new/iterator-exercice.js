// Type JavaScript here and click "Run Code" or press Ctrl + s
console.log( "Hello, world!" );

// CHALLENGE 1

function sumFunc( arr ) {
	let value = 0;
	for ( let i = 0; i < arr.length; i++ ) {
		value += arr[ i ];
	}
	return value;

}

// Uncomment the lines below to test your work
const array = [ 1, 2, 3, 4 ];
console.log( sumFunc( array ) ); // -> should log 10

function returnIterator( arr ) {
	let index = 0;
	return () => {
		const el = arr[ index ];
		index += 1;
		return el;
	};
}

// Uncomment the lines below to test your work
const array2 = [ "a", "b", "c", "d" ];
const myIterator = returnIterator( array2 );
console.log( myIterator() ); // -> should log 'a'
console.log( myIterator() ); // -> should log 'b'
console.log( myIterator() ); // -> should log 'c'
console.log( myIterator() ); // -> should log 'd'


// CHALLENGE 2

function nextIterator( arr ) {
	return {
		index: 0,
		next() {
			let el = arr[ this.index ];
			this.index += 1;
			return el;
		}
	};

}

// Uncomment the lines below to test your work
const array3 = [ 1, 2, 3 ];
const iteratorWithNext = nextIterator( array3 );
console.log( iteratorWithNext.next() ); // -> should log 1
console.log( iteratorWithNext.next() ); // -> should log 2
console.log( iteratorWithNext.next() ); // -> should log 3
//

// CHALLENGE 3

function sumArray( arr ) {
	const iterator = nextIterator( arr );
	let value,
			result = 0;
	while ( ( value = iterator.next() ) ) {
		result += value;
	}
	return result;
}

// Uncomment the lines below to test your work
const array4 = [ 1, 2, 3, 4 ];
console.log( sumArray( array4 ) ); // -> should log 10


// CHALLENGE 4

function setIterator( set ) {
	let iterator = set[ Symbol.iterator ]();
	return iterator;
}

// Uncomment the lines below to test your work
const mySet = new Set( "hey" );
const iterateSet = setIterator( mySet );
console.log( iterateSet.next() ); // -> should log 'h'
console.log( iterateSet.next() ); // -> should log 'e'
console.log( iterateSet.next() ); // -> should log 'y'


// CHALLENGE 5

function indexIterator( arr ) {
	return {
		index: 0,
		next() {
			const el = [ this.index, arr[ this.index ] ];
			this.index += 1;
			return el;
		}
	};

}

// Uncomment the lines below to test your work
const array5 = [ "a", "b", "c", "d" ];
const iteratorWithIndex = indexIterator( array5 );
console.log( iteratorWithIndex.next() ); // -> should log [0, 'a']
console.log( iteratorWithIndex.next() ); // -> should log [1, 'b']
console.log( iteratorWithIndex.next() ); // -> should log [2, 'c']


// CHALLENGE 6

function Words( string ) {
	this.str = string;
}

Words.prototype[ Symbol.iterator ] = function () {
	let data = this.str.match( /\b(\w*).\b/g );

	return {
		index: 0,
		next() {
			if ( this.index == data.length ) {
				return { done: true };
			}
			const el = { value: data[ this.index ] };
			this.index += 1;
			return el;
		}
	};
};

// Uncomment the lines below to test your work
const helloWorld = new Words( "Hello World" );
for ( word of helloWorld ) { console.log( word ); } // -> should log 'Hello' and 'World'

// CHALLENGE 7

function valueAndPrevIndex( array ) {
	let index = 0;
	return {
		sentence: () => {
			if ( index == 0 ) {
				var el = `first ${ index }`;
			}
			else {
				var el = `was found after ${ index - 1 }`;
			}
			index += 1;
			return el;
		}
	};

}

const returnedSentence = valueAndPrevIndex( [ 4, 5, 6 ] );
console.log( returnedSentence.sentence() );
console.log( returnedSentence.sentence() );
console.log( returnedSentence.sentence() );


//CHALLENGE 8

function* createConversation( string ) {
	setInterval( function* () { yield "toto"; }, 1000 );
}

console.log( createConversation( "english" )
.next() );


//CHALLENGE 9
function waitForVerb( noun ) {

}

async function f( noun ) {

}

f( "dog" );

