"use strict";

function lotteryNum() {
	return ( Math.round( Math.random() * 100 ) % 58 ) + 1;
}

// that's pure function
function pickNumber( number, newNumber ) {
	if ( !number.includes( newNumber ) ) {
		number = [
			...number, newNumber
		].sort( ( a, b ) => a - b );
	}
	return number;
}

var luckyLotteryNumbers = [];

while ( luckyLotteryNumbers.length < 6 ) {
	luckyLotteryNumbers = pickNumber(
			Object.freeze( luckyLotteryNumbers ),
			lotteryNum()
	);
}

console.log( luckyLotteryNumbers );
