"use strict";

const get3 = () => 3;
const get5 = () => 5;

// that's a composition
const add = ( nb1, nb2 ) => nb1 + nb2;
// that's a
const add2 = ( func1, func2 ) => add( func1(), func2() );

console.log( add( get3(), get5() ) );
console.log( add2( get3, get5 ) );

const got = ( v ) => () => v;

// classic one
const addn = ( ...func ) => {
	let start = 0;

	for ( let f of func ) {
		start += f();
	}

	return start;
};

const KyAddn = ( ...func ) => {

	while ( func.length > 2 ) {

		let [ fn0, fn1, ...rest ] = func;

		func = [
			function f() {
				return add2( fn0, fn1 );
			},
			...rest
		];
	}
	return add2( func[ 0 ], func[ 1 ] );
};

const testArr = [ got( 22 ), got( 22 ), got( 1 ), got( 4423 ) ];

console.log( addn( ...testArr ) );

// recursion one
const r_addn = ( ...func ) => {
	if ( func.length <= 1 ) {
		return func[ 0 ]();
	}

	let value = func[ 0 ]();
	func.shift();
	return value + r_addn( ...func );
};

console.log( r_addn( ...testArr ) );

console.log(
		testArr.reduce( ( composedFn, fn ) => {
			return () => add2( composedFn, fn );
		} )()
);


const oddEven = [ 22, 44, 1, 2, 3, 4, 4, 1, 4, 2, 422, 11, 1 ];

console.log( oddEven.reduce( ( acc, v ) => {
	if ( acc.includes( v ) === false ) {
		return [ v, ...acc ];
	}
	return acc;
}, [] ) );

console.log( oddEven.filter( ( nb ) => !( nb % 2 ) ) );


const newt = oddEven.map( ( nb ) => () => nb );
console.log( addn( ...newt ) );
































