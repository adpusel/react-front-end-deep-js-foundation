"use strict";

// take string and return function
// first call will always be a string
// function strBuilder( str ) {
//
// 	const text = "" + str;
//
// 	return function ( arg ) {
// 		if ( typeof arg !== "string" ) {
// 			console.log( text );
// 			return text;
// 		}
//
// 		return function () {
// 			const text = text;
// 			return strBuilder;
// 		};
// 	};
// }

function strBuilder( str ) {
	return function next( v ) {
		if ( typeof v == "string" ) {
			str += v;
			return next;
		}
		return str;
	};
}


function strBuild( str ) {
	return function next( v ) {
		if ( typeof v == "string" ) {
			return strBuild( str + v );
		}
		return str;
	};
}


// return a function call each time an element is return
// if no string are given, we return a string with all other string

var hello = strBuilder( "Hello, " );
hello();
var kyle = hello( "Kyle" );
var susan = hello( "Susan" );
var question = kyle( "?" )();
var greeting = susan( "!" )();

console.log( kyle() );
console.log( susan() );
console.log( question );
console.log( greeting );

// console.log( strBuilder( "Hello, " )( "" )( "Kyle" )( "." )( "" )() === "Hello, Kyle." );
// console.log( hello() === "Hello, " );
// console.log( kyle() === "Hello, Kyle" );
// console.log( susan() === "Hello, Susan" );
// console.log( question === "Hello, Kyle?" );
// console.log( greeting === "Hello, Susan!" );
