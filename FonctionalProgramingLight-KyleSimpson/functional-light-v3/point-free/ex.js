"use strict";

// function output( txt ) {
// 	console.log( txt );
// }

// c'est pour ca que en rx je peux ecrire: (o) => console.log(o)  -> console.log
var output = console.log;

function not( fn ) {
	return function neg( ...argv ) {
		return !fn( ...argv );
	};
}

function isShortEnough( str ) {
	return str.length <= 5;
}

var isLongEnough = not( isShortEnough );


var msg1 = "Hello";
var msg2 = msg1 + " World";


// function printIf( shouldPrintIt ) {
// 	return function ( msg ) {
// 		if ( shouldPrintIt( msg ) ) {
// 			output( msg );
// 		}
// 	};
// }

function when( fn ) {
	// predicate are function that return true of false.
	return function ( predicate ) {
		return function ( ...args ) {
			if ( predicate( ...args ) ) {
				return fn( ...args );
			}
		};
	};
}

// 'when' is ship by a lib.
const printIf = when( output );


printIf( isShortEnough )( msg1 );		// Hello
printIf( isShortEnough )( msg2 );
printIf( isLongEnough )( msg1 );
printIf( isLongEnough )( msg2 );		// Hello World

// or
when( output )( isShortEnough )( msg1 );


























