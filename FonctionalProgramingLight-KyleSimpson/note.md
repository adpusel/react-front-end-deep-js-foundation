# Introduction

Kyle, says that functional programming is a lot about __mathematic__
so, I will dig more into after the course. And when I tried to
understand what functional is, he crash into hard math and give up.

The course name is Functional light because, Kyle start by coding,
by the bottom rather than by the top, ( the mathematical way ).

Kyle says that the motto: if you can explain something you didn't catch it
is partly wrong. Because when you understand something so well, you can't
explain it, that's begin part of you. And that's true for functional Programming.

Kyle use the story of learning hiking, and the name of the prise.

## Why Functional Programing ?
Or the difference between Imperative vs Declarative.

#### Imperative Programming
Is About :
  * How to do something think, example, if I want to make an addition on
    a indefinite number of nb, I add to an accumulator all the nb that
    I grab with my loop.

  * The code is imperative, because the reader need for understand
    read all the code. And that's a problem, because our brain are
    not good at that! This is a computer stuff!

  * All that create more bug, because the reader need to read the code
    to understand the what.

#### Declarative Programming
Is About:
  * Why, I do that, I focus on the output, and hide to the reader what I do

  * Code needs to communicate a story, it's need to explain the mind
    set up of the writer.

  * We need to focus on why I do that? If the code is not enough
    explaining, use comment to explain why.

  * Functional programming is about doing more Declarative code.


#### Functional programming Journey
That's will be hard, I will says Why I even a programmer. I start crying
and, after a while, I will got all of that stuff! And be very happy!
That's will takes decades to understand how solves problem differently.
I will go in the pit of despair...

#### Provable

FP is about Math, Kyle use a story about how we use math to calculate
the bill at the restaurant. We start by thinking, 1 + 1 = 2, but did
we know why? We don't.But we take advantage of that, every day.

But functional programmer are going a particular form of joy to explain
the proof :).

Kyle talk about TDD, __I will implement TDD for all my next project__
because without that I never get a good programmer.

I write the test, but the test do not passed, and I do a little modification
on my code, and retry it... I really know what I'm doing, The test should
pass at the first try. So there is something that I do not get, I only
control partially the pb.

Functional pb is based on real and hard math, FP is about use math
principle with confidence to control 100% of the code.

A better code, is a code where I need to read less, as FP land on
math principle, we less explain.


#### Course Overview
* I will learn what is a function
* Closure
* Composition, the flow
* Immutability, how the state change over time
* Recursion, that's ok, like regex, it's cool to doing that.
* Lists / Data Structures, here will came filter and map.
* Async, How I interact with the time factor, we will use Observable.
* FP Library, we will look at cool lib.

Kyle end the course, speaking of his talk: "code is for human".


# Function

#### Function vs Procedures
FP is not about the function keyword

These elements are clearly taking input,
transform them before print them.

The bar to be a function is higher than that.
these are procedures, It doesn't mean because it uses functions
keyword, that this is a function, there is no return keyword, that's
not a function!

```javascript
// Not return something.
function addNumber(x = 0, y = 0, z = 0, w = 0) {
  var total = x + y + z + w;
  console.log(total)
}

// Call a Procedure
function extraNumbers(x = 2, ...args) {
  return addNumber(x, 20, ...args);
}

extraNumbers(); // 42
extraNumbers(3, 8, 11); // 62

```

A function has to return some output, if it doesn't have a return keyword
it's definitely not a function.

Function can only be call by function, if a function is call
by a Procedure, that became a Procedure. So It will no came to our party
the Functional One. I can't do FP with a thing that is not a function.

Here, I don't care about the array of returning my tuple.
I only want the value, I use a destructuring to explain that.

So, function : __take inputs__ return __output(s)__
~~~javascript

function tuple(x, y) {
  return [ x + 1, y - 1];
}

var [ a, b ] = tuple( ...[ 5, 10 ] );
a; // 6
b; // 9
~~~

!! if the previous one return only one element, and we loose the logic
   between input and output, that also __not a function__

#### function name semantics
` f(x) = 2x^2 + 3 ` these number are actually noting before we put
that on a calculator and get the corresponding curve.

We will put inside of f(x), the function of x.
We give the x and the function give us the y.
So what is a function ?
__A function is the relationship between the input and the output__

A __function Name describe__ to the reader the __relationship between
inputs and outputs__.

If there is not relation between the input and the output of my function,
it's not functional programming.

Kyle description: (compute -> calculer)
__Function: the semantic relationship between input and computed output__

```javascript
// here the correlation between all the input is obvious.
function shippingRate(size, weight, speed) {
  return ( (size + 1) * weight ) + speed
}
~~~

Think before start writing my function, is it an function, is it returning
something? Is it a relationship between his input and his output?

So undefined is a valid return, if I have a function that search
a props inside my object, and return undefined, that's valid.


#### Side Effect:

This code is __a lot like our program__ it takes information in some part
use it, and update other part in our program.

This is a procedure, it's interact with the outer world!
__input and output have to be direct__.

Indirect input / output are side effect. __Side effect is not a problem.__
That just not a function, that's can't be prove with a mathematical theory,
the f(x) can be, it's a function.
__Math doesn't work if any of the input/output are indirect__.

A function can't affect the outer world.

```javascript
function shippingRate(undefined) {
	rate = ( ( size + 1 ) * weight ) + speed
}

var rate;
var size = 12;
var weight = 4;
var speed = 5;
shippingRate();
rate; // 57;

size = 8;
speed = 6;
shippingRate();
rate; // 42
~~~

##### What are really side effect?
* any I/O (console, file, etc) is side effects.
* Database is side effect
* Network call
* DOM
* Timestamps
* Random numbers...
* CPU Heat ( change the system, so modify it )
* CPU Time Delay ( my program block running other program )

__It's impossible to avoid side effect !__
__Avoid side effect == minimise side effect__ !!
One program without side effect is indistinguishable from no program.

so when we __doing side effect__, this need to be very __obvious__ and intentional!
because bug happen mostly where there are side effect!

#### Pure function
A pure function is a function that receive only direct input
and return direct output, without __any side effect__ ( almost )

But that's not about the function definition, but about the function call.
Because of __context__ in js, we will talk about __pure function call__

```javascript
// pure 
function addTwo(x, y) {
  return x + y;
}

// impure
function addAnother(x, y) {
  return addTwo(x, y) + z; // add z is impure
}

// so I can got that
addAnother(20, 21) // 42 ? what??
~~~
the z variable came from `we don't know` so, that begin unpredictable
and the function begin impure!
Now, we can't prove the math.

Seen code like that is a warning, you need to think, why there is
access variable from outside?

__BUT__

Here the function is pure, z will never be change, it's a constant.
So this begin predictable!
And even if z is a var, and it's never reassign, even,
if __is purpose is to be use like a const__ using it inside function
keep the function pure.
```javascript
const z = 1;

function addTwo(x, y) {
  return x + y;
}

function addAnother(x, y) {
  return ( addTwo(x + y) + z)
}
~~~
haha, but, addTwo is also a variable, and it can also be change...
The only thing that matters, is, is the purpose of that var to be const.
That's the key:
__the reader needs to render obvious when the function is pure__
Because when it's the case, we just need to read the line to understand
what it's doing. But in procedure, to handle side effect,
we read and mentally execute all previous line to get the current state
of the program at this specific point of time.

#### Reducing the surface area
By returning a closure, we here ensure the reader that z is a const
for that function. Only 2 line can modify the z,
__the surface area of what can fuck z is little__
we reduce the confidence of the reader, and get that code more readable.
```javascript
function addAnother(z) {
  return function addTwo(x, y) {
    return x + y + z;
  }
}

addAnother(1)(20, 21) // 42
~~~

#### Same input, Same Output
I need to see only relevant part of the program to understand
if the input of my function make it pure.
The more I can reduce the surface area,
the more my code will be readable and simpler.
```javascript
function getId(obj) {
  return obj.id
}

getId({
  get id() {
  	return Math.random()
  }
})
~~~
__pure function is when a function, for given input,
always return same output__

#### Extracting impurity
If the function talks with the db, we can't do nothing, we need that!
But with a function that do computation and write then in the db,
we can separate it in two, the first pure one, computes.
Second procedure, write in the db.
__we extract the impurity of the data flow__
```javascript
function addComment(userID, comment) {
  var record = {
  	id: uniqueId(),
  	userID,
  	comment
  }
  var element = buildCommentElement(record);
  commentsList.appendChild(element);
}

addComment(42, "This is my first comment!")
~~~

__begin__

```javascript
function newComment(userId, commentId, comment) {
  var record = {
  	id: commentId,
  	userId,
  	text: comment
  }
  
  return buildCommentElement(record)
}

var commentId = uniqueId();
var element = newComment(
		42,
		commentId,
		"This is my first comment!"
)
commentsList.appendChild(element);
```

#### Contain impurity
I copy the global scope in my function, I work on it, I return it,
so I interact with impurity only 2 times.

__that pattern is an adapter__
```javascript
var SomeAPI = {
	threshold: 13,
	isBelowThreshold(x) {
		return x <= SomeAPI.threshold
	}
}
var numbers = [];

function insertSortedDesc(v) {
  SomeAPI.threshold = v;
  var idx = numbers.findIndex(SomeAPI.isBelowThreshold)
  if (idx == -1) {
  	idx = numbers.length;
  }
  numbers.splice(idx, 0, v);
}

function getSortedNums(nums, v) {
  var [origNumber, origThreshold] = [numbers, SomeAPI.threshold];
  numbers = nums.slice()
  insertSortedDesc(v);
  nums = numbers;
  [numbers, SomeAPI.threshold] = [origNumber, origThreshold];
  return nums;
}

numbers = getSortedNums(numbers, 3);
numbers = getSortedNums(numbers, 4);
~~~

That's is very ugly and super hard to maintain.
By example, if we use that with the dom tree,
we have to save the dom, modify it and put the new state.
Impossible to do that with database.
__The stuff here is, we save original value,
  we do our modification, and we set again them.__
By this trick, the values get the const statue. :).
We contain impurity.

getSortedNums begin pure function, direct input / output, and no sideEffect

To __contain impurity__, I have 2 options : __wrapper and adapter__.



## Argument Adapter

#### function argument
Parameters and argument are different,
argument get assigned to parameters :
```javascript
function f(parameter, ...) {}
f(arguments...)
```

The shape of a function is the type and the number of its inputs
and the type and the number of its outputs.
```javascript
// the shape is unary
// 1 input / output
function increment(x) {
  return sum(x, 1);
}

// binary 2 input / 1 output
function sum(x, y) {
  return x + y;
}
~~~

Shape describe the i/o signature, this is super important,
but I do not get why.

__95% of all function in good FP program will be of these 2 kind.__

All other function are unary function,
but more the function as input more it's hard.

#### Arguments Shape Adapters
High order function, if a function has function in his i/o,
that's an HOF. We can use adapters to reduce the number of argument
that take a function, to get unary or binary function.

__Shape adapter__
```javascript
function unary(fn) {
  return function one(arg) {
    return fn(arg)
  }
}

function binary(fn) {
  return function two(arg1, arg2) {
    return fn(arg1, arg2)
  }
}

function f(...arg) {
  return arg
}

var one = unary(f);
var two = unary(f);

one(1,2,3,4) // 1
two(1,2,3,4) // 2
~~~

Almost all utility in lodash or Ramda are this kind of hof.

#### Flip & Reverse Adapter

```javascript
function flip(fn) {
  return function flipped(arg1, arg2, ...args) {
    return fn(arg2, arg1, ...args);
  }
}

function f(...args) {
  return args;
}

var g = flip(f)

g(1,2,3,4) // 2, 1, 3, 4

// reverse
function reverse(f) {
  return function(...args) {
  	return f(args.reverse())
  }
}
~~~

Functional programmer don't want create new utilities,
he wants transform the input in the output he knows how handle
using different hof / utilities. We use lego brick, always the same
to create, share and give other best readability.
__We, in FP use only lego SAME lego bricks__


#### Spread Adapter
take an array of arguments and apply one by one, like the .apply() fn
is call in FP, an __apply__.

```javascript
function spreadArgs(fn) {
  return function spread(args) {
    return fn(...args);
  }
}

function f(x, y, z, w) {
  return x + y + z + w;
}

var g = spreadArgs(f)

g([1,2,3,4]) // 10
```

## Point Free

#### Equational Reasoning

Fix point function : if you have more than one input,
you fix one at in defined value:
f(x, y) = z, we fix x at 3
f(3, y) = z.
It's defining a function without defining it's point (input).
And that's begin define function by using other function. :).


person is the input,
in mathematical term, it's called a point for onPerson function


these functions are the shame shape,
that mean their are interchangeable

```javascript
getPerson(function onPerson(person) {
  return renderPerson(person)
})

// became
getPerson(renderPerson);
~~~

Doing that transformation is Equational Reasoning.
That's sound cool man!

If two function have the same shape, they are interchangeable!

#### Point free refactor

Here, the guide that push to use !isOdd and not v % != 1,
is not DRY, __is to make the relation between these 2 shape, obvious__

The point is v, can we define it point free?
We want removing the declarative style, impose by the point.

But how, we will use and HOF.
```javascript
function isOdd(v) {
  return v % 2 == 1;
}

function isEven(v) {
  return !isOdd(v);
}

isEven(4);
~~~

in FP style:

```javascript
function not(fn) {
  return function negated(...args) {
  	return !(fn(args))
  }
  
}

function isOdd(v) {
	return v % 2 == 1;
}

var isEven = not(isOdd)
~~~

in FP libraries, that's call complement.
That code show less, but with it,
it's clearer that's this two function have relationship

#### Advanced Point Free
FP programmer take a lot care of the shape of the function,
and that includes the order of __its arguments__.

And there are more __natural order__.
```javascript
function mod(y) {
  return function forX(x) {
    return x % y;
  }
}

function eq(y) {
  return function forX(x) {
    return x === y;
  }
}

var mod2 = mod(2)
var eq1 = eq(1)

// or
mod(2)(1)

function isOdd(x) {
  return eq1( mod2 ( x ) );
}
~~~

mod2 take on __input less__ than mod, it is more __specialise__
__we progressively change to operator from function__

give directly output of one function to another, is call : __composition__
take one function input and make it output to other function.

here the compose utility:
```javascript
var mod2 = mod(2)
var eq1 = eq(1)

function isOdd(x) {
  return eq1( mod2( x ) )
}

function compose(fn2, fn1) {
  return function compose(v) {
    return fn2 ( fn1( v ) )
  }
}

var isOdd = compose(eq1, mod2)

var isOdd = compose(eq(1), mod(2));
~~~
that's awesome, this is exactly what rx do under the hood with pipe,
this is so cool. That's composition!

__isOdd and compose have the same shape, so isOdd is composition__
so I can use composition to build isOdd.


## Closure

little reminder:

```javascript
function makeCounter() {
  var counter = 0;
  return function increment() {
    return ++counter;
  }
}

var c = makeCounter()

c(); // 1
c(); // 2
c(); // 3
~~~

that breaks one off the pure function rule,
we do not have the same output.
__closure is not necessarily pure function__
the rule is, __if I close a variable, I can't reassign__


this is a closure, unary keep access to the input, that is a function.
```javascript
function unary (fn) {
    return function one(arg) {
      return fn(arg);
    }
}
~~~

bad exemple of clojure:
```javascript

function strBuilder(str) {
  return function next( v ) {
   if (typeof v == 'string') {
   	  str += v;
   	  return next;
   }
   return str;
  }
}

var hello = strBuilder( "Hello, ");
hello() // Hello, 
var kyle = hello( "Kyle" ); // Hello, Kyle
var susan = hello( "susan" ); // Hello KyleSusan ?? WTF
~~~

there is only one single closure, when I create strBuilder in hello.
str refers to only one space, and for that, modify it at one place
modify it at all place.
__I do not execute function, I do not create new closure space__

But in the next I will create a new execution context each time
because I __execute my callback__ and that's awesome!

```javascript
function strBuilder( str ) {
	return function (s) {
		if ( typeof s === "string" ) {
			return strBuilder(str + s);
		}
		return str;
	};
}

// if str is 'toto', that will execute:

function strBuilder( undefined + 'toto' ) {
	return function (s) {
		if ( typeof s === "string" ) {
			return strBuilder('toto' + s);
		}
		return 'toto';
	};
}

// that will became : 
function(s) {
  if ( typeof s === 'string' )
  	return setBuilder('toto' + s)
  return 'toto'
}
~~~

Je prerempli pour la prochain execution,
ma function setBuilder s'execute a chaque iteration et __return
le callback prerempli avec str de set__.
__je ne modifie jamais la variable dans mon context, je return la modif__

#### Lazy vs Eager

##### Lazy
```javascript
function repeater(count) {
  return function allTheAs() {
    return "".padStart(count, "A")
  }
}

var A = repeater(10);

A(); // AAA ...
A(); // AAA ...
```

I build the string at A(), I set up all the stuff in my function
but the __compute start with the A()__.
that's __lazy / defer__ we defer until the function is call.
Why we defer ? If that function is only call 10% of the time,
we do not want have unnecessary computation.

##### Eager
the computation happens in second call
```javascript
function repeater( count ) {
	const memory = "".padStart( count, "A" );
	return function () {
		return memory;
	};
}
~~~

#### Memoization
Lazy or eager, that's hard position, so : memoization

that code is pure, it return always the same output.
But, it's not obvious that's this will the case,
we reassign the str variable, and that's no FP.
__this code style lead to a low degree of confidence__
```javascript
function repeater( count ) {
	var str;
	return function allTheAs() {
		if ( str === undefined ) {
			str = "".padStart( count, "A" );
		}
		return str;
	};
}
~~~

To keep the trust, we use memoize from FP lib,
it keeps trace of the inputs, caches output.
if the i are the same, its return cache.
```javascript
function repeater(count) {
  return memoize(function allTheAs() {
    return "".padStart(count, "A")
  })
}
~~~
memoization is expensive in resources, so when know we use it?
* if you expect to be call multiple time with the same input

#### Referential Transparency
the definition :
__if I can replace any function by his return without altering execution,
I have a pure function and a Referential Transparency__
R haskell

That allow the reader, to mentally associate the function call,
to the same value, and give better review.

#### Generalized to Specialized
I want to only display to the reader necessary data

The pattern here is to specialise more and more the function,
and make the relation between them obvious.
ajax -> getCustomer -> getCurrentUser ...

__the order matter a lot, General -> Specific__
That's why in the map function, the function is before the array,
the array is more specific, the function can be use multiple time.
```javascript
function ajax( url, data, cb ) {...}

ajax( CUSTOMER_API, { id: 42 }, renderCustomer );

function getCustomer( data, cb ) {
	return ajax( CUSTOMER_API, data, cb );
}

getCustomer( { id: 42 }, renderCustomer );

function getCurrentUser( cb ) {
	return getCustomer( { id: 42 }, cb );
}

getCurrentUser( renderCustomer );
~~~

#### Partial Application & Currying

##### Partial
```javascript
function ajax( url, data, cb ) {}

var getCustomer = partial( ajax, CUSTOMER_API );
var getCurrentUser = partial( getCustomer, { id: 42 } );

getCustomer( { id: 42 }, renderCustomer );
getCurrentUser( renderCustomer );
~~~

##### Currying
Curring is a really common form of specialisation
```javascript
function ajax( url ) {
	return function getData( data ) {
		return function getCB( cb ) {};
	};
}

// full call
ajax( CUSTOMER_API )( { id: 42 } )( renderCustomer );

// specialisation : 
var getCustomer = ajax( CUSTOMER_API );
var getCurrentUser = getCustomer( { id: 42 } );
~~~

with lib utilities:
```javascript
function ajax( url, data, cb ) {}

var aj = url => data => cb => ajax( url, data, cb );

var curry_ajax = curry(
		3,
		function ajax( url, data, cb ) {}
);

var getCustomer = ajax( CUSTOMER_API );
var getCurrentUser = getCustomer( { id: 42 } );
~~~

What a great adapter! OMG!


#### Partial vs currying
* Both are specialization techniques
* Partial Application presets some arguments now,
  receives the rest at each call
* Currying receives each argument one at time

##### strict vs loos currying
All lib allow to do loose curring, that's no because performance,
because convenience.
```javascript
var ajax = curry(
		3,
		( url, data, cb ) => undefined
);

// strict
ajax( CUSTOMER_API )( { id: 42 } )( render );

// looooooose 
ajax( CUSTOMER_API, { id: 42 } )( render );
~~~


#### Changing function shape with curry
In the code below, add is binary and map take only unary function.
so the addOne is an adapter even better,
addOne __specialise__ add

Specialisation change the shape, curry also.
make them both in one line!
```javascript
function add( x, y ) {return x + y;}

[ 0, 2, 4, 6, 8 ].map( function addOne( v ) {
	return add( 1, v );
} );

add = curry( add );

[ 0, 2, 4, 6, 8 ].map( add( 1 ) );

~~~

## Composition
That's a composition!!

Composition is abstraction,
but __abstraction__ is __not hiding__
it's __separation__.

```javascript
function minus2( x ) { return x - 2;}
function triple( x ) { return x * 3;}
function increment( x ) {return x + 1;}

// add shipping rate
var tmp = increment( 4 );
tmp = triple( tmp );
totalCost = basePrice + minus2( tmp );
~~~

Kyle talks about the candy factory, and how generate more candy with
the same space area ( remember the pretty drawing ).
The only way ? Is to add more machine ?

let's build some composition to accelerate the process:
```javascript
function minus2( x ) { return x - 2;}
function triple( x ) { return x * 3;}
function increment( x ) {return x + 1;}

// add  shipping rate
totalCost = 
  bestPrice + 
  minus2(triple(increment(4)));
~~~

and the better, is when we separate how to / what to
I __create abstraction__ from my __composition__
that's the machine with single input, single output
```javascript
function minus2( x ) { return x - 2;}
function triple( x ) { return x * 3;}
function increment( x ) {return x + 1;}

// what to
function shippingRate(x) {
  return minus2(triple(increment(x)))
}

// how to
totalCost = 
  basePrice +
  shippingRate(4)
~~~

And after that, the boss ask to build a machine,
that build other machine, with that we will be able to change
the candy output each day.

so I'll make myself a utility

it's clear here, that __composition is declarative dataflow!__
```javascript
function composeThree(fn3, fn2, fn1) {
  return function compose(v) {
    return fn3( fn2 ( fn1 (v) ) )
  };
}

function minus2( x ) { return x - 2;}
function triple( x ) { return x * 3;}
function increment( x ) {return x + 1;}

var shippingRate = 
  composeThree(minus2, triple, increment)

// what to
totalCost = 
  basePrice +
  shippingRate(4)
~~~

__Composition is vital, and super important, because our entire program
is one data flow, for that all FP lib provide a compose function__

#### Piping vs composition
pipe : __goes left to right__
compose: __goes right to left__
remember with the exercise.

#### Associativity

That's an mathematical concept
means I can compose what I want,
if I have always the same order,
that's will be always the same result.

```javascript
function minus2( x ) { return x - 2;}
function triple( x ) { return x * 3;}
function increment( x ) {return x + 1;}

function composeTwo(fn2, fn1) {
  return function composed(v) {
    return fn2(fn1(v))
  }
}

var f = composeTwo(
		composeTwo(minus2, triple),
		increment
)
// is equal to 

var ff = composeTwo(
		minus2,
		composeTwo(triple, increment)
)
~~~

#### Composition with Currying
```javascript
let sum = ( x, y ) => x + y;
const triple = ( x ) => x * 3;
let divBy = ( y, x ) => x / y;

divBy( 2, triple( sum( 3, 5 ) ) );

sum = curry(2, sum);
divBy = curry(2, sum);

composeThree(
		divBy(2),
		triple,
		sum(3)
)(5)
~~~

## Immutability
def : __things don't change unexpectedly__
That's doesn't mean the program will not change,
the idea is control changes.
__Assignment Immutability__ :
    if I assign something, I can't reassign it

#### Value immutability
That what we talking about!
99% come from a value that mutated not how we expect.
The real problem is we the value mutates,
not when the value is reassigned.

We can't be sure that processOrder will not modify orderDetail,
and create a bug. Has FP we see that and protect ourselves with
pattern when they can't happen.

__warranty to the reader of that code, that the object will not change.__
```javascript
{
	const orderDetails = {
		orderId: 42,
		total: (basePrice + shipping)
	};
	
	if ( orderItems.length > 0) {
		orderDetails.items = orderedItems
	}

	// here is like : please create a bug for me!
	processOrder(orderDetails);
}
~~~

#### Object.freeze
Most of the time, I don't want immutability,
I want set on lecture only my data,
to be sure of that integrity
```javascript
{
	const orderDetails = {
		orderId: 42,
		total: (basePrice + shipping)
	};
	
	if ( orderItems.length > 0) {
		orderDetails.items = orderedItems
	}

	!! I freeze bitch!
	processOrder(Object.freeze(orderDetails));
}
~~~
2 good stuffs:
  * Object.freeze will throw __exception__ if I __modify__ that object.
  * I __assure the reader__ that this __object is not modify__ by this utility function.

For kyle, the most important feature is to assure the reader that,
the object is locked.

#### Don't mutate, Copy!!
Read-Only Data structures:
    Data structures that never need to be mutated.

We can't be sure that the reader has locked the reference that he passed
to us, so we need to copy all the value that we receive in our program.
```javascript
function processOrder( order ) {
	var processedOrder = { ...order };
	if ( !( "status" in order ) ) {
		processedOrder.status = "complete";
	}
	saveToDatabase( processedOrder );
}
~~~

#### Immutable Data Structure
When I need a mutable data structure, what I need is actually an
immutable Data structure. Because we want structured mutation.

How do that ?

We can __access__ to the structure only with the __an API__. The API provide
control on __unexpected data changes__. You can't change data structure,
you can only create a __new data structure__ with the __change apply on it__.

But that will came with cost, in term of resource. So we use lib that
can handle this copy without kill the cpu. The lib create new object
and have ptr on the previous state of the object that have no modification.
And we can disable this skill temporary if performance matters.

#### Immutable.js
```javascript
var items = Immutable.List.of(
		textbook,
		supplies
)

var updateItems = items.push(calculator);

item === updateItems // false

items.size // 2
updateItems.size // 3
~~~



## OMG recursion !
To write good recursion code, you need to add first understand the
problem, and write it.
```JAVASCRIPT
function countVowels(str) {
  if (str.length == 0) return 0;
  var first = (isVowel(str[0]) ? 1 : 0);
  return first + countVowels( str.slice(1) )
}

// or better, because it avoid one unusefull function call.

function betterCount(str) {
	let first = (isVowel(str[0]) ? 1 : 0)
	if (str.length <= 1) return first;
	return first + betterCount(str.slice(1))
}

countVowels("saoethu saouoeu irlgrlgc,.p,voeu");
~~~

#### Recursion in production:
and the fear of stack overflow exceeded!
Kyle talks about what I already know from 42 ( best school ever)

#### Optimization: Tail Calls
That's a technique to prevent to much call :).
If the function is at the end of the stack,
and call the next one as is place,
that use only one call at the time.

That's is not for performance perspective,
even that's will be sometime slower.
But we will optimise our memory's gestion,
to prevent error!

A real steakOver flow, restart the system immediately,
there is nothing possible to prevent that.

So in the js engine, there is a protection against that,
because, some people can use that to crash the user's device.

with IE6 the max number of call was 13.

modern browser use other techniques, but the fact is,
we don't know how stack call we can make and, more that's
can be different between all the js engine (firefox or chrome).

#### Proper Tails Calls PTC
PTC = 0(1) memory space.
TCO = family of potential optimizations, that engineer decide to do or not to do

we need the __return keyword__ before a __function call__ to start PTC,

```javascript
"use strict" // without that no PTC

function decrement(x) {
  return sub(x, 1); // that is an PTC 
}
~~~

Nothing is less expensive for JsEngine than a function call,
so the js engine do automatically the PTC like in that more
realist example

```javascript
"use strict"

function diminish(x) {
  if (x > 90) {
  	return diminish(Math.trunc(x / 2)); // diminish is a PTC
  }
  return x - 3;
}

diminish(367) // 42
~~~

#### Refactoring to PTC form
Has there is an first with the return operation,
this is not a PTC.
```javascript
function countVowels(str) {
  var first = (isVowel(str[0]) ? 1 : 0);
  if (str.length <= 1) return first;
  return first + countVowels(str.slice(1))
}
countVowels("aonuhtsaoneuheaohun");
~~~

If I can't save first in the function, the only other place where I can
is in the function, call has arguments:

```javascript
function countVowels(count, str) {
  count += (isVowel(str[0]) ? 1 : 0);
  if (str.length <= 1) return count;
  return countVowels(count, str.slice(1))
}
countVowels(0, "sanouhsaoneuhsaoneuh");
~~~

But that's no really clean, so we will create an interface

That's a funny way to show yo, that I will save this number in closure
```javascript
var countVowels = curry(2, function countVowels(count, str) {
  count += (isVowel(str[0]) ? 1 : 0);
  if (str.length <= 1) return count;
  return countVowels(count, str.slice(1))
})(0)

countVowels("anoetuhaosneuh");
~~~

if I run the previous code in safari (that have the PTC) Y will never
get out of memory, but that's will be slower than the loop version.

#### Continuation-Passing Style. CPS
the cont function is an __identity function__,
its __returns what we pass__ to it.

That a cheat, an elegant one, we build each time a bigger function
that's will be computed at the end. We cheat the Js engine, because
we fill the heap, but the heap can be fuck to, and we kill the system...
So that's cool but dangerous!
```javascript
"use strict"

function countVowels(str, cont = v => v) {
  var first = (isVowel(str[0]) ? 1 : 0);
  if (str.length <= 1) return cont(first);
  return countVowels(str.slice(1), function(v) {
    return cont(first + v)
  })
}
~~~

#### Trampolines
This is the good technical, it's a funny way to create something
that will back and forth. The function call a function, second return
callback, the first execute callback, that return callback...
We never create a stack.
The stack newer goes beyond 1.
Because we return function that'll be the next call.

This will be provide by the lib but here a simple example:
While I return function, I loop on. :).
```javascript
function trampoline(fn) {
  return function trampolined(...args) {
    var result = fn(...args);
    
    while (typeof result == "function") {
    	result = result()
    }
    
    return result;
  }
}
~~~

What's it look like with our vowel :
The only difference is we wrap all our recursive call in return function
```javascript
var countVowels = 
  trampoline(function countVowels(count, str) {
    count += (isVowel(str[0]) ? 1 : 0);
    if (str.length <= 1) return count;
    return function() {
    	return countVowels(count, str.slice(1));
    }
  })
// optionally
countVowels = curry(2, countVowels)(0)
~~~

// the pb here is that's I can't add these to some element, these are
block by all that shit.

## List operation
We start by using these only on the list,
but the purpose is to use that on all the existing structure!

#### Map: Transformation
There is a heavy term for that,
a functor : __a values ( or a collection of value ) over it witch those
values in it can be mapped__
any structure that have a map operator is functor.
Functor => can be mapped, that's it!

Map is a __transformation operation__,
single rule, we always end up with the same structure, not only array !

```javascript
function makeRecord(name) {
  return { id: uniqId(), name }
}

function map(mapper, arr) {
  var newList = []
  for (let elem of arr) {
  	newList.push( mapper(elem) );
  }
  return newList;
}

map(makeRecord, ["Kyle", "Susan"]);
// [{ id: 42, name: "Kyle"}, {id: 728, name: "Susan"}]
~~~

Has array is the most common way to do that, inside the prototype,
there is .map


#### filter
I know how that's works, thank to rx !
We only select value with some criteria
```javascript
function isLoggedIn(user) {
  return user.session != null;
}

function filterIn(predicate, arr) {
  var newList = [];
  for (let elem of arr) {
  	if ( predicate(elem) ) {
  		newList.push(elem)
  	}
  }
  return newList
}
~~~

#### Reduce
Reduce combine value. It's the most use utility in FP.
We use reduce to produce value, or even structure.
Kyle repeats that the shape of a function is very important,
and here more because, every binary function can be use in a reducer.
```javascript
function addToRecord( record, [ key, value ] ) {
	return { ...record, [ key ]: value };
}

function reduce( reducer, initialVal, arr ) {
	var ret = initialVal;
	for ( let elem of arr ) {
		ret = reducer( ret, elem );
	}
	return red;
}

reduce( addToRecord, {}, [
	[ "name", "Kyle" ],
	[ "age", 39 ],
	[ "isTeacher", true ]
] );
~~~

#### Composition with reduce
reduce is like, a form of composition
```javascript
function add1() { return v + 1};
function mul2() { return v * 2};
function div3() { return v / 3};

function composeTwo(fn2, fn1) {
  return function composed(v) {
    return fn2( fn1( v ) );
  };
}

var f = [div3, mul2, add1].reduce(composeTwo);
// reverse reduce
var p = [add1, mul2, div3].reduceRight(composeTwo);

f(8); // 6
p(8); // 6


function composition(...fns) {
  return function composed(v) {
    return fns.reduce(v, (func, value) => func(value))
  }
}
~~~

#### Fusion
For performance perspective, do that is not good,
because we will throw away a lot of data in a link of map // filter.

```javascript
function add1() { return v + 1}
function mul2() { return v * 2}
function div3() { return v / 3}

var list = [2,7,2,1,2];

list
.map( add1 )
.map( mul2 )
.map( div3 );

// or very better
list.map(
		compose(div3, mul2, add1)
)
~~~
In purpose of have better purity, the .map function is linked to his owner
__use map ship by FP lib__.


## Transduction
if we want to link filters and reducers ?
With transducing, this is the more hard !!
That about to use together map, filter and reduce.
We need to mathematically transform these function,
in shape that they can be compatible.

transducing == composition of reducer.
We want to convert the mappers, and predicates into reducers
That will reshaping the function into a reducer

the mapReducer, create a prototype reducer,
that need to be passe into a reducer, to begin a real reducer
```javascript
function add1(v) { return v + 1}
function isOdd(v) { return v % 2 === 1}
function sum(total, v) { return total + v}

var list = [2,7,2,1,2];

list
.map( add1 )
.map( isOdd )
.map( sum );

// these utility are give by the lib
var transducer = compose(
		mapReducer(add1),
		filterReducer(isOdd)
)

transduce(
		transducer,
		sum,
		0,
		list
)

// helper to do the same
// into will automatically chose the best end up function, here as we
// provide number, a sum function.
into(transducer, 0, list);
~~~

#### Deriving Transduction
first step, we reduce inside the function
```javascript
function add1(v) { return v + 1}
function isOdd(v) { return v % 2 === 1}
function sum(total, v) { return total + v}

var list = [2,7,2,1,2];

list
.map( add1 )
.filter( isOdd )
.reduce( sum );



function mapWithReduce(arr, mappingFn) {
  return arr.reduce(function reducer(list, v) {
    list.push( mappingFn(v) );
    return list;
  }, []);
}

function filterWithReduce(arr, predicateFn) {
	return arr.reduce(function reducer(list, v) {
		if (predicateFn(v)) list.push(v);
		return list;
	}, []);
}

list = mapWithReduce( list, add1);
list = filterWithReduce( list, isOdd );
~~~

2sd step: we return a reducer
__all the function are now the same SHAPE!__
```javascript
function mapReducer(mappingFn) {
  return function reducer(arr, v) {
    arr.push(mappingFn (v))
    return arr;
  }
}

function filterReducer(predicateFn) {
  return function reducer(list, v) {
    if (predicateFn(v)) list.push(v);
    return list;
  }
}

var list = [1,2,13]

list
.reduce(mapReducer(add), [])
.reduce(filterReducer(isOdd), [])
.reduce(sum);
// 42
~~~

I can simplify that by :

```javascript
function listCombination(list, v) {
  list.push(v)
  return list;
}

function mapReducer(mappingFn) {
  return function reducer(list, v) {
  	return listCombination(list, mappingFn(v));
  }
}

function filterReducer(predicateFn) {
  return function reducer(list, v) {
    if (predicateFn(v)) return listCombination(list, v);
    return list;
  }
}

[1, 2, 4, 4, 2]
.reduce(mapReducer(add1), [])
.reduce(filterReducer(isOdd), [])
.reduce(sum)
~~~

became with curry operator

```javascript

function listCombination(list, v) {
  list.push(v)
  return list;
}

var mapReducer = curry(2, function mapReducer(mappingFn, combineFn) {
  return function reducer(list, v) {
    return combineFn(list, mappingFn(v))
  }
})

var filterReducer = curry(2, function filterReducer(predicateFn, combineFn) {
  return function reducer(list, value) {
    if (predicateFn(value)) return combineFn(list, v)
    return list;
  }
})


[1, 2, 4, 4, 2]
.reduce(mapReducer(add)(listCombination), [])
.reduce(filterReducer(isOdd)(listCombination), [])
.reduce(sum)
~~~

last one:

```javascript
function listCombination(list, v) {
  list.push(v);
  return list;
}

var mapReducer = curry(2, function mapReducer(mappingFn, combineFn) {
	return function reducer(list, v) {
	  return combineFn( list , mappingFn(v) );
	}
})

var filterReducer = curry(2, function filterReducer(predicateFn, combineFn) {
  return function reducer(list, v) {
    if (predicateFn(v)) return combineFn(list, v);
    return list;
  }
})

var transducer = compose( mapReducer(add), filterReducer(isOdd) );

var list = [1, 2, 3, 4];

list
.reduce( transducer( listCombination ), [])
.reduce( sum )

// sum and listCombination have the same shape,
// so we can do:

list.reduce(transducer(sum()), 0);
~~~

## Data Structure Operations

#### Introduction .
```javascript
var obj = {
	name: "Kyle",
	email: "Getify@gmail.com"
};

function mapObj(mapper, o) {
  var newObj = {};
  for (let key of Object.keys(o)) {
  	newObj[key] = mapper( o[key] )
  }
  return newObj;
}

mapObj(function lower(val) {
  return val.toLowerCase()
}, obj);
~~~

#### Monad Data Structures
Monad is a way to create a friendly functional structure.
Monad turn value into functor.
def : __Monad: a pattern for pairing data with a set of predictable
behaviors that let it interact with other data + behavior pairing
(monads)__

Just is the simplest monad, it's a wrapper around a single value.
It's a single discreet entity. Like with map, If I map a monad, I get
another monad.

It's an object, with 3 methods and some closure.
It's not that hard, it's a method that give value
ways to interact with other values.
```javascript
function Just( val ) {
	return { map, chain, ap };

	// monad return monad
	function map( fn ) {
		return Just( fn( val ) );
	}

	// aka: bind, flatMap
	function chain( fn ) {
		return fn( val );
	}

	// we use the map of another monad with our value,
	// map need function, so our value are function
	function ap( anotherMonad ) {
		return anotherMonad.map( val );
	}
}
~~~

#### Just monad
```javascript
var fortyOne = Just(41);
// that's another monad
var fortyTwo = fortyOne.map((value) => value + 1)

const identity = (value) => value;

fortyOne.chain(identity) // 41
fortyTwo.chain(identity) // 42


var user1 = Just("Kyle");
var user2 = Just("Susan");

var tuple = curry(2, function tuple(x,y){
	return [x,y]
})

var users = user1
.map(tuple)
// here a temporary monad with curry("kyle) is stock
.ap(user2)  ;

users.chain(identity) // ["Kyle", "Susan"]
~~~


#### The Maybe Monad
```javascript
var someObj = { something: { else: { entirely: 42 } } };

// When you have Nothing monad, You stuck on nothing!
// This mean : no-op, stop!
function Nothing() {
  return { map: Nothing, chain: Nothing, ap:Nothing}
}

var Maybe = { Just, Nothing, of: Just}

// if there're value, return a monad with the value, otherwise return Nothing
function fromNullable(val) {
  if (val == null) return Maybe.Nothing();
  else return Maybe.of(val)
}

// that's allow to safely extract object props as monad
const prop = curry(2, function prop(prop, obj) {
  return fromNullable(obj[prop])
})

// that a protection, because if we get Nothing,
// it has the .chain, and return Nothing that has .chain...g
Maybe.of(someObj)
.chain( prop ("something" ))
.chain( prop ("else" ))
.chain( prop ("entirely" ))

// debug
.chain(identity) // 42 :)

~~~

## Async
```javascript
var a = [1, 2, 3];

var b = a.map(function double(v) {
  return v * 2;
});
~~~

## FP libraries

#### Lodash/fp
Lodash/FP is not lodash.
If I use lodash before, use it, not for me.

#### Ramda
That's the recommandation of Kyle.



















