# intro
Kyle talks about when it's change of stack with hope the new one will solve
his problem.
So he begin to thinks that there a some universal problem, that every
developer got.
 
Coder are professional guessers, we are like weathermen. 
we write code that work or did not work, and we do not know why?
This create a big problem, if you do not know how your code works,
you don't have power to fix it if it breaks. 
We all in the same boat.

So when the code does not work, by ego we say: I rewrite, and all will be better
We rewrite perpetually code, because we do not understand how the first works,
if 100 line are written, 10000 line are rewrite, or do the same.


Typically example is an if without else class.
```javascript
if (isLoggedIn()) {
   session = pullFromCache(); 
} 
// what, no else here!?
```
Here, I know why there is no else, everthing is logic, 
but I did not explain to the other human why,
and that's the more important stuff. 
The pupose of the code is to explain the mental bubble of the writer.


The big problen is that we're writing code for the computer, not for human like: 
```javascript
const a = 6 * 4 - 4

const b = ( 6 * 4 ) - 4 // here, linter says, no need (), but human love
                        // that kind of readability!
```
We optimise the code for the computer, in place of optimising the code
for the reader.

We think we write fastest code, but we only give indication to the
computer, the compilation will follow its own rules.

The real purpose of code, is for communicating ideas with other people.

We face an existential crisis, that we keep trying to compete against the
computer against the thing we design it to be best at.
It evolves faster than we are.

All the tools are about create the faster stuff, but no one is about,
that framework is not the fastest, but it is the best understandable.

We spend, 70% of our time to read code, the average number of line push by day
by programmers is __10__. so : 
__code must first be read before it can be written__

If my code has to be rewritten to be fixed, improved or extended, I failed

Take 6 min of every hour to think about, is that variable has a good name?
Is that function to big ... Always imagine, if I have to explain that piece of 
code, how I will do that?

Enterprise spend 50% of their budget to fix old pieces of code, that's a lot!

Readability is the hole point, each minute to clean the code, code that will be
read 70% more that write, I gain exponentially time.

The one thing we will always be better than the computer: 
empathetic communication with other people.


























